#include "mainctrl.h"

MainCtrl::MainCtrl()
{
    //создаем 2 обьекта что контролируют графическую часть
    qDebug() << QTime::currentTime();
    window = new WidgetManager;
    //и модельную(часть данных)
    model  = new ModelManager;

    //Window connect
    QObject::connect(window, SIGNAL(signalGetAddSticker(MemberStick*)),
                     model,  SLOT  (slotGetAddSticker  (MemberStick*)));

    QObject::connect(window, SIGNAL(signalGetChangePassword(QStringList)),
                     model,  SLOT  (slotGetChangePassword  (QStringList)));

    QObject::connect(window, SIGNAL(signalGetDoSynchronize()),
                     model,  SLOT  (slotGetSynchronize()));

    QObject::connect(window, SIGNAL(signalGetEditSticker(MemberStick*, bool)),
                     model,  SLOT  (slotGetEditSticker  (MemberStick*, bool)));

    QObject::connect(window, SIGNAL(signalGetListNotice(int,QStringList)),
                     model,  SLOT  (slotGetListNotice  (int,QStringList)));

    QObject::connect(window, SIGNAL(signalGetLogin(QString,QString)),
                     model,  SLOT  (slotGetLogin  (QString,QString)));

    QObject::connect(window, SIGNAL(signalGetLogout()),
                     model,  SLOT  (slotGetLogout()));

    QObject::connect(window, SIGNAL(signalGetNotice()),
                     model,  SLOT  (slotGetNotice()));

    QObject::connect(window, SIGNAL(signalGetRegistration(QStringList)),
                     model,  SLOT  (slotGetRegistration  (QStringList)));

    QObject::connect(window, SIGNAL(signalGetRemoveSticker()),
                     model,  SLOT  (slotGetRemoveSticker()));

    //Model connect
    QObject::connect(model,  SIGNAL(signalSetAddSticker(QString)),
                     window, SLOT  (slotSetAddSticker  (QString)));

    QObject::connect(model,  SIGNAL(signalSetChangePassword(QString)),
                     window, SLOT  (slotSetChangePassword  (QString)));

    QObject::connect(model,  SIGNAL(signalSetEditSticker()),
                     window, SLOT  (slotSetEditSticker()));

    QObject::connect(model,  SIGNAL(signalSetListNotice(QList<MemberStick*>)),
                     window, SLOT  (slotSetListNotice  (QList<MemberStick*>)));

    QObject::connect(model,  SIGNAL(signalSetLogin(QString,QString,QString)),
                     window, SLOT  (slotSetLogin  (QString,QString,QString)));

    QObject::connect(model,  SIGNAL(signalSetNotice()),
                     window, SLOT  (slotSetNotice()));

    QObject::connect(model,  SIGNAL(signalSetRegistration(QString)),
                     window, SLOT  (slotSetRegistration  (QString)));

    QObject::connect(model,  SIGNAL(signalSetRemoveSticker()),
                     window, SLOT  (slotSetRemoveSticker()));

    QObject::connect(model,  SIGNAL(signalSetSynchronize()),
                     window, SLOT  (slotSetSynchronize()));

    QObject::connect(model,  SIGNAL(signalSetNetworkStatus(bool, int)),
                     window, SLOT  (slotSetNetworkStatus  (bool, int)));

    QObject::connect(model,  SIGNAL(signalSetImmediateMemory(QList<MemberStick*>)),
                     window, SLOT  (slotSetImmediateMemory  (QList<MemberStick*>)));

    //проверка на авторизацию последнего польщователя
    //если он вышел перед закрытием программы
    //тогда устанвливаеться NoName
    QStringList list = model->local->firstStart();
    if (list.at(2) == "")
        window->slotSetLogin(list.at(0), list.at(1), list.at(2));
    else
        window->slotGetLogout();
    qDebug() << QTime::currentTime();
}

//после создания всего вызываеться сигнал что все создали(вроде логично)
void MainCtrl::endCreate()
{
    emit signalEndStart();
}

