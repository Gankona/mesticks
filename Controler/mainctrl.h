#ifndef MAINCTRL_H
#define MAINCTRL_H

#include <Model/modelmanager.h>
#include <View/MainInterface/widgetmanager.h>

#include <QtCore/QDebug>
#include <QtCore/QTimer>

class MainCtrl : public QObject
{
    Q_OBJECT
public:
    MainCtrl();
    WidgetManager *window;
    ModelManager *model;

    bool isLogin;

    void endCreate();
    void firstLogin();

signals:
    void signalEndStart();
};

#endif // MAINCTRL_H
