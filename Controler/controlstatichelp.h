#ifndef CONTROLSTATICHELP_H
#define CONTROLSTATICHELP_H

#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QString>
#include <QtCore/QStringList>

class ControlStaticHelp
{
public:
    ControlStaticHelp();
    static QString testNamePole  (QString poleText,
                                  QString buttonCalled);
    static QStringList readQss   (QString nameTheme);
};

#endif // ENTERINGCONTROL_H
