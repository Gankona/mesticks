#include "controlstatichelp.h"

ControlStaticHelp::ControlStaticHelp(){}

QString ControlStaticHelp::testNamePole(QString poleText,
                                        QString buttonCalled)
{
    //проверка на коректность ввода полей
    //поля не должны быть пустые
    if (poleText == "")
        return "*поле " + buttonCalled + " обязательно для заполнения.";
    else {
        bool isCorrect = true;
        //проверка поля електронного адресса
        //должно быть {какой то символ}@{символ}.{символ}
        if (buttonCalled == "e-mail"){
            int mailSymbol = 0;
            bool textBefore = false;
            bool textAfter = false;
            for (int i = 0; i < poleText.length(); i++){
                if (mailSymbol == 0){
                    if (poleText[i].isLetterOrNumber()
                                            || poleText[i] == '.')
                        textBefore = true;
                }
                if (mailSymbol == 1){
                    if (poleText[i].isLetterOrNumber()
                                            || poleText[i] == '.')
                        textAfter = true;
                }
                if (poleText[i] == '@')
                    mailSymbol++;
            }

            for (int i = 0; i < poleText.length(); i++)
                if (!(poleText[i].isLetterOrNumber()
                        || poleText[i] == '@'
                        || poleText[i] == '.'))
                    isCorrect = false;
            if (mailSymbol != 1)
                return "*в поле " + buttonCalled + " введено или ни одного,"
                        " или несколько знаков @";
            else if (isCorrect && textAfter && textBefore)
                return "correct";
            else
                return "*в поле " + buttonCalled + " введено некоректный символ,"
                        " допустимы только цифры и символы алфавита";
        }
        //проверка что бы ничего не было кроме цифр и букв
        else {
            for (int i = 0; i < poleText.length(); i++)
                if (!poleText[i].isLetterOrNumber())
                    isCorrect = false;

        //возвращаем что все отлично
        if (isCorrect)
            return "correct";
        //возвращаем информацию об ошибке
        else
            return "*в поле " + buttonCalled + " введено некоректный символ,"
                    " допустимы только цифры и символы алфавита";
        }
    }
}

//чтение стиля с файла и отправка его в настройки в прототип стилей
QStringList ControlStaticHelp::readQss(QString nameTheme)
{
    QStringList ret;
    ret.clear();

    QFile file(":/qss/Files/Qss/" + nameTheme + ".css");
    file.open(QIODevice::ReadOnly);
    QString lastStr = file.readLine();
    for (int i = 0; i < 3; i++){
        lastStr = file.readLine();
        QString str = "";
        do {
            str += lastStr;
            lastStr = file.readLine();
        } while(lastStr.at(0) != '/');
        ret << str;
    }
    file.close();
    return ret;
}
