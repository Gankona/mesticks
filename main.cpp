#include <Controler/mainctrl.h>
#include <QtWidgets/QSplashScreen>
#include <QtWidgets/QApplication>

int main(int argc, char **argv){
    QApplication app(argc, argv);

    //создание екрана заставки
    QSplashScreen splash(QPixmap(":/images/Files/Images/trayIcon.png"));
    splash.show();

    //создание самого приложения
    MainCtrl *ctrl = new MainCtrl();

    QObject::connect(ctrl, SIGNAL(signalEndStart()), &splash, SLOT(close()));
    QObject::connect(ctrl, SIGNAL(signalEndStart()), ctrl->window->tray, SLOT(show()));

    //вызов последнего этапа отрисовки программы
    //тут авторизация и скрытие сплеш-скрина
    ctrl->endCreate();

#ifdef Q_OS_ANDROID
    ctrl->window->widget->show ();
#endif
    //ставим флаг, что при закрытии главного окна не закрывалось все приложение
    QApplication::setQuitOnLastWindowClosed(false);

    return app.exec();
}
