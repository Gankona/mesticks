#ifndef LOCALMANAGER_H
#define LOCALMANAGER_H

#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <QtCore/QObject>

#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

class LocalManager
{
public:
    LocalManager();

    QSqlDatabase db;
    QSqlQuery *query;
    QString login;
    QString name;

    bool isConnectDb;
    bool isAvtorizate;

    bool connectToDB();
    QStringList firstStart();
    QStringList loginFunc (QString login, QString password);
    void addAccountToLocal(QString queryStr, QString login);
    void logout();
};

#endif // LOCALMANAGER_H
