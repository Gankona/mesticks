#ifndef SERVERMANAGER_H
#define SERVERMANAGER_H

#include <QtNetwork/QTcpSocket>

#include <QtCore/QDataStream>
#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtCore/QTime>

class ServerManager : public QObject
{
    Q_OBJECT
public:
    ServerManager();
    QTcpSocket *socket;
    QDataStream stream;
    QTime startPing;

    int pingTime;
    bool isPing;
    bool isConnecting;

    void sendToServerReg(QStringList str);
    void sendToServerLogin(QString login, QString password);
    void sendToServerChangePass(QStringList changePass);

public slots:
    void slotConnect();
    void slotPostFromServer();
    void slotUpdateConnecting();

signals:
    void signalSetNetworkStatus(bool, int);
    void signalFromServerReg(QStringList);
    void signalFromServerLogin(QStringList);
    void signalFromServerChangePass(QStringList);
};

#endif // SERVERMANAGER_H
