#ifndef STICKLIST_H
#define STICKLIST_H

#include <QtCore/QDateTime>
#include <QDebug>

class StickList
{
public:
    StickList(QString str,
              QDateTime dTime,
              bool isCompleteSend);
    QString name;
    QDateTime time;
    bool isComplete;
    int id;
};

#endif // STICKLIST_H
