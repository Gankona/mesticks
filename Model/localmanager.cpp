#include "localmanager.h"

LocalManager::LocalManager()
{
    isAvtorizate = false;
    isConnectDb = connectToDB();
    if (! isConnectDb){
        qDebug() << "Sorry, we can't connect to DataBase";
    }
    else {
        qDebug() << "Connect to DataBase";
    }
    query = new QSqlQuery(db);
}

//вызов функции происходит после запускаа приложения
QStringList LocalManager::firstStart()
{
    QStringList ret;
    ret.clear();

    //создает БД если ёё еще нет
    if (isConnectDb){
        query->exec("CREATE TABLE IF NOT EXISTS Users "
                    "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    "name TEXT, password TEXT, login TEXT, "
                    "email TEXT, isLogin INT DEFAULT (1));");
        query->exec("SELECT name, login FROM Users WHERE isLogin = 0;");
        query->next();

        //проверяет есть ли польщователи
        if (!query->isNull(0) && query->isNull(2)){
            name = query->value(0).toString();
            login = query->value(1).toString();
            if ( !(name == "") && !(login == "")){
                isAvtorizate = true;
                ret << name << login << "";
            }
            else {
                ret << "" << "" << "Какое то поле пустое";
            }
        }
        //создания дефолтного польщователя и таблиц к нему
        else {
            ret << "" << "" << "Количество равно не единицы";
            query->exec("CREATE TABLE IF NOT EXISTS NoName (id INTEGER PRIMARY KEY AUTOINCREMENT,"
                        "type INTEGER DEFAULT (0),title STRING, image STRING, list INTEGER DEFAULT (-1),"
                        "text TEXT (1, 10000), timeEnd DATETIME, timeStart DATETIME,"
                        "isComplete BOOLEAN DEFAULT FALSE);");
            query->exec("CREATE TABLE IF NOT EXISTS NoNameList (\"group\" INTEGER, title STRING,"
                        "isComplete BOOLEAN DEFAULT FALSE, timeEnd DATETIME,"
                        "id INTEGER PRIMARY KEY AUTOINCREMENT);");
        }

    }

    //вывод ошибки
    else {
        ret << "" << "" << "Связь с серверомs отсутствует";
        qDebug() << ret.at(2);
    }

    return ret;
}

//подсоединение к БД
bool LocalManager::connectToDB()
{
    QDir dir(QDir::current());
    if ( ! dir.cd("DataBase"))
        dir.mkdir("DataBase");

    dir.cd("Database");
    qDebug() << dir.absolutePath();
    qDebug() << dir.path();
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dir.absolutePath() + "/ClientDB");
    bool ok = db.open();
    return ok;
}

//добавление в БД информации про пользователя которого там нет
void LocalManager::addAccountToLocal(QString queryStr, QString login)
{
    query->exec(queryStr);
    //создание таблицы заметок для данного пользователя
    query->exec("CREATE TABLE IF NOT EXISTS "
                + login
                + " (id INTEGER PRIMARY KEY AUTOINCREMENT, type INTEGER DEFAULT (0),"
                "title STRING, image STRING, list INTEGER DEFAULT (- 1),"
                "text TEXT (1, 10000), timeEnd DATETIME, timeStart DATETIME,"
                "isComplete BOOLEAN DEFAULT FALSE);");

    //создания списка для данного пользователя
    query->exec("CREATE TABLE IF NOT EXISTS "
                + login
                + "List (\"group\" INTEGER, title STRING,"
                "isComplete BOOLEAN DEFAULT FALSE, timeEnd DATETIME,"
                "id INTEGER PRIMARY KEY AUTOINCREMENT);");
}

//вызываеться при авторизации
QStringList LocalManager::loginFunc(QString loginSend,
                               QString passwordSend)
{
    QStringList ret;
    ret.clear();
    QString password = "crashAppliction";

    isAvtorizate = false;
    if (isConnectDb){
        query->exec("SELECT name, password FROM Users"
                    " WHERE login = '" + loginSend + "';");
        query->next();
        if (query->isNull(0))
            ret << "" << "" << "Аккаунт не найден";
        else {
            name = query->value(0).toString();
            login = loginSend;
            password = query->value(1).toString();
            if ( !(name == "") && !(login == "")){
                if (password == passwordSend){
                    isAvtorizate = true;
                    query->exec("UPDATE Users SET isLogin = 0 WHERE login = '"
                                + login +"';");

                    ret << name << login << "";
                }
            else
                    ret << "" << "" << "Пароль неверен";
            }
        }
    }
    else
        ret << "" << "" << "Связь с серверомs отсутствует";
    return ret;
}

//вызываеться при выходе из учетной записи
void LocalManager::logout()
{
    query->exec("UPDATE Users SET isLogin = 1 WHERE isLogin = 0");
    isAvtorizate = false;
    login = "NoName";
    qDebug() << "i'm out";
}
