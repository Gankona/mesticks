#ifndef MODELMANAGER_H
#define MODELMANAGER_H

#include <Model/localmanager.h>
#include <Model/memberstick.h>
#include <Model/servermanager.h>

#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtGui/QImage>

class ModelManager : public QObject
{
    Q_OBJECT
public:
    ModelManager();
    ServerManager *server;
    LocalManager *local;

public slots:
    void slotGetLogin(QString login, QString password);
    void slotGetLogout();
    void slotGetChangePassword(QStringList changePass);
    void slotGetRegistration  (QStringList regInfo);

    void slotGetAddSticker (MemberStick *member);
    void slotGetEditSticker(MemberStick *member, bool isEdit);
    void slotGetListNotice (int type, QStringList parametr);
    void slotGetNotice();
    void slotGetSynchronize();
    void slotGetRemoveSticker();

    void slotSetReg  (QStringList list);
    void slotSetLogin(QStringList list);
    void slotSetChangePass(QStringList result);

    void slotSetNetworkStatus(bool isConnect, int ping);
    void slotGetImmediateMemory();

signals:
    void signalSetLogin(QString, QString, QString);
    void signalSetRegistration  (QString);
    void signalSetChangePassword(QString);

    void signalSetAddSticker(QString);
    void signalSetEditSticker();
    void signalSetListNotice(QList<MemberStick *> member);
    void signalSetNotice();
    void signalSetSynchronize();
    void signalSetRemoveSticker();

    void signalSetNetworkStatus(bool, int);
    void signalSetImmediateMemory(QList<MemberStick *> member);
};

#endif // MODELMANAGER_H
