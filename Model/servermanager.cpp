#include "servermanager.h"

ServerManager::ServerManager()
{
    //создаем сокет
    socket = new QTcpSocket(this);
    isConnecting = false;
    QObject::connect(socket, SIGNAL(connected()), this, SLOT(slotConnect()));
    QObject::connect(socket, SIGNAL(readyRead()), this, SLOT(slotPostFromServer()));
}

//отлавливаем соединение с сервером
void ServerManager::slotConnect(){
    //связываем поток с открытым сокетом
    stream.setDevice(socket);
    isConnecting = true;
    isPing = true;
    //отправляем в интерфейс информацию про статус соединения
    emit signalSetNetworkStatus(isConnecting, 0);
}

//обработка сигналов от сервера
void ServerManager::slotPostFromServer()
{
    QString varQuery;
    stream >> varQuery;
    qDebug() << varQuery;
    qDebug() << "post From Server 1";

    //регистрация
    if (varQuery == "registration"){
        QStringList regInfo;
        stream >> regInfo;
        qDebug() << "post From Server 2" << regInfo;
        emit signalFromServerReg(regInfo);
    }

    //авторизация
    else if (varQuery == "login"){
        qDebug() << "post login";
        QStringList ret;
        ret.clear();
        stream >> ret;
        qDebug() << ret;
        emit signalFromServerLogin(ret);
    }

    //смена пароля
    else if (varQuery == "changePassword"){
        qDebug() << "post changePassword";
        QStringList ret;
        ret.clear();
        stream >> ret;
        qDebug() << ret;
        emit signalFromServerChangePass(ret);
    }

    //возвратный пинг что выслал клиент
    else if (varQuery == "pingC"){
        isPing = true;
        pingTime = startPing.msecsTo(QTime::currentTime());
    }

    //пинг что выслал сервер и его возврат ему назад
    else if (varQuery == "pingS"){
        stream << (QString)"pingS";
    }
}

//отправка информации для авторизации
void ServerManager::sendToServerLogin(QString login, QString password)
{
    qDebug() << "sendToServerLogin" << login << password;
    stream << (QString)"login" << login << password;
}

//отправка информации для регистрации
void ServerManager::sendToServerReg(QStringList str)
{
    qDebug() << "sendToServerReg" << str;
    stream << (QString)"registration" << str;
}

//отправка информации для смены пароля
void ServerManager::sendToServerChangePass(QStringList changePass)
{
    qDebug() << (QString)"changePassword" << changePass;
    stream   << (QString)"changePassword" << changePass;
}

//обновление статуса соединения
void ServerManager::slotUpdateConnecting()
{
    QTimer::singleShot(1000*2, this, SLOT(slotUpdateConnecting()));
    emit signalSetNetworkStatus(isConnecting, pingTime);
    ///если пинг что последним отправили пришел назад
    /// тогда высылаем еще один пинг
    /// иначе закрываем сокет и отсоединяем поток от него
    if (isConnecting){
        if (isPing){
            startPing = QTime::currentTime();
            isPing = false;
            stream << (QString)"pingC";
        }
        else {
            stream.unsetDevice();
            socket->disconnectFromHost();
            isConnecting = false;
        }
    }
    //пробуем подключиться к серверу
    else {
        stream.unsetDevice();
        socket->connectToHost("127.0.0.1", 41993);
    }
}
