#ifndef MEMBERSTICK_H
#define MEMBERSTICK_H

#include <Model/sticklist.h>

#include <QtCore/QDebug>
#include <QtGui/QImage>

class MemberStick
{
public:
    MemberStick();

    QDateTime timeEnd;
    QDateTime timeStart;
    QImage *image;
    QString text;
    QString title;
    QList <StickList*> stickList;

    bool isCompleteMestick;
    int groupList;
    int number;
    int type;
};

#endif // MEMBERSTICK_H
