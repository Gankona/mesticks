#include "modelmanager.h"

ModelManager::ModelManager()
{
    //создаеться серверная и локальная часть управления БД
    server = new ServerManager;
    local = new LocalManager;

    //отлавливаем запросы что пришли от сервера
    QObject::connect(server, SIGNAL(signalFromServerChangePass(QStringList)),
                     this,   SLOT  (slotSetChangePass         (QStringList)));

    QObject::connect(server, SIGNAL(signalFromServerLogin(QStringList)),
                     this,   SLOT  (slotSetLogin         (QStringList)));

    QObject::connect(server, SIGNAL(signalFromServerReg(QStringList)),
                     this,   SLOT  (slotSetReg         (QStringList)));

    QObject::connect(server, SIGNAL(signalSetNetworkStatus(bool, int)),
                     this,   SLOT  (slotSetNetworkStatus  (bool, int)));

    //запуск сокета что подсоединяеться к серверу
    server->slotUpdateConnecting();
    slotGetImmediateMemory();
}

//добавление заметок
void ModelManager::slotGetAddSticker(MemberStick *member)
{
    //создаем строку что отправиться в БД
    QString query = "INSERT INTO ";
    query += local->login;
    query += " (type, title, image, list, text, timeEnd, timeStart)";
    query += " VALUES (";
    query += QString::number(member->type);
    query += ", '";
    query += member->title;
    query += "', '";
    if (member->type%2 == 1){}
    else
        query += "NULL";
    query += "', ";
    query += "-1";
    query += ", '";
    if (((int)(member->type/4))%2 == 1)
        query += member->text;
    query += "', '";
    if (((int)(member->type/8))%2 == 1)
        query += member->timeEnd.toString("yyyy-MM-dd hh:mm:ss");
    query += "', '";
    query += member->timeStart.toString("yyyy-MM-dd hh:mm:ss");
    query += "');";

    //вытягиваем свободный номер для списка
    bool isCorrect;
    isCorrect = local->query->exec(query);
    qDebug() << query << isCorrect;
    local->query->exec("SELECT id FROM " + local->login + " WHERE timeStart = '"
                       + member->timeStart.toString("yyyy-MM-dd hh:mm:ss") + "';");
    local->query->next();
    int id = local->query->value(0).toInt();
    local->query->exec("UPDATE " + local->login
                    + " SET list = " + QString::number(id)
                    + " WHERE id = " + QString::number(id) + ";");
    //создания списка
    foreach (StickList *stick, member->stickList){
        query = "INSERT INTO ";
        query += local->login;
        query += "List (\"group\", title, timeEnd)";
        query += " VALUES (";
        query += QString::number(id);
        query += ", '";
        query += stick->name;
        query += "', '";
        query += stick->time.toString("yyyy-MM-dd hh:mm:ss");
        query += "');";
        bool isCorrect;
        isCorrect = local->query->exec(query);
        qDebug() << query << isCorrect;
    }

    //вывод результат добавление заметки
    if (local->isAvtorizate)
        emit signalSetAddSticker(tr("Заметка добавлена"));
    else
        emit signalSetAddSticker(tr("Заметка добавлена в общее хранилище"));
}

//смена пароля
void ModelManager::slotGetChangePassword(QStringList changePass)
{
    if (local->isAvtorizate){
        if (server->isConnecting)
            server->sendToServerChangePass(QStringList({local->login,
                                                        changePass.at(0),
                                                        changePass.at(1)}));
        else
            slotSetChangePass(QStringList({"", tr("Отсутствует связь с сервером")}));
    }
    else
        slotSetChangePass(QStringList({"", tr("Вы не авторизированы")}));
}

//отправка обновления заметки
void ModelManager::slotGetEditSticker(MemberStick *member, bool isEdit)
{
    if (isEdit){
        if (member->stickList.length() != 0){}
        QString query = "UPDATE ";
        query += local->login;
        query += " SET type = ";
        query += QString::number(member->type);
        query += ", title = '";
        query += member->title;
        query += "', image = '";
        if (member->type%2 == 1){}
        else
            query += "NULL";
        query += "', text = '";
        if (((int)(member->type/4))%2 == 1)
            query += member->text;
        query += "', timeEnd = '";
        if (((int)(member->type/8))%2 == 1)
            query += member->timeEnd.toString("yyyy-MM-dd hh:mm:ss");
        query += "', timeStart = '";
        query += member->timeStart.toString("yyyy-MM-dd hh:mm:ss");
        query += "' WHERE id = ";
        qDebug() << "modelManager id = " << member->number;
        query += QString::number(member->number);
        query += ";";
        bool isCorrect;
        isCorrect = local->query->exec(query);
        qDebug() << query << isCorrect;

        //создание списка
        foreach (StickList *stick, member->stickList){
            if (stick->id == -1){
                query = "INSERT INTO ";
                query += local->login;
                query += "List (\"group\", title, timeEnd)";
                query += " VALUES (";
                query += QString::number(member->number);
                query += ", '";
                query += stick->name;
                query += "', '";
                query += stick->time.toString("yyyy-MM-dd hh:mm:ss");
                query += "');";
                bool isCorrect;
                isCorrect = local->query->exec(query);
                qDebug() << query << isCorrect << " -1";
            }
            //обновление списка
            else {
                query += "UPDATE ";
                query += local->login;
                query += "List SET \"group\" = ";
                query += QString::number(member->number);
                query += ", title = '";
                query += stick->name;
                query += "', timeEnd = '";
                query += stick->time.toString("yyyy-MM-dd hh:mm:ss");
                query += "' WHERE id = '";
                query += QString::number(member->number);
                query += "';";
                bool isCorrect;
                isCorrect = local->query->exec(query);
                qDebug() << query << isCorrect << " 0";
            }
        }
    }

    //обновления главной части заметки
    else {
        QString query = "UPDATE ";
        query += local->login;
        query += " SET isComplete = '";
        if (member->isCompleteMestick)
            query += "TRUE";
        else
            query += "FALSE";
        query += "' WHERE id = ";
        query += QString::number(member->number);
        query += ";";
        bool isCorrect;
        isCorrect = local->query->exec(query);
        qDebug() << query << isCorrect;

        //обновление списка заметки
        foreach (StickList *stick, member->stickList){
            QString query = "UPDATE ";
            query += local->login;
            query += "List SET isComplete = '";
            if (stick->isComplete)
                query += "TRUE";
            else
                query += "FALSE";
            query += "' WHERE id = ";
            query += QString::number(stick->id);
            query += ";";
            bool isCorrect;
            isCorrect = local->query->exec(query);
            qDebug() << query << isCorrect;
        }
    }
}

//извлечения заметок с БД
void ModelManager::slotGetListNotice(int type, QStringList parametr)
{
    qDebug() << "ModelManager::slotGetListNotice";
    QList<MemberStick *> member;
    member.clear();

    switch (type){
    //считываеться для извлечения времени заметки
    case -1:
    //считыветься для отображение заметки в режиме просмотра
    case 0:{
        bool isCorrect = local->query->exec("SELECT type, title, image,"
                           "list, text, timeEnd, timeStart, id,"
                           "isComplete FROM " + local->login + ";");
        qDebug() << "resultQuery" << isCorrect << "SELECT type, title, image,"
                    "list, text, timeEnd, timeStart, id,"
                    "isComplete FROM " + local->login + ";" << isCorrect;
        while (local->query->next()){
            member.push_back(new MemberStick());
            member.last()->number = local->query->value(7).toInt();
            qDebug() << "id = " << member.last()->number;
            member.last()->title = local->query->value(1).toString();
            member.last()->timeStart = local->query->value(6).toDateTime();
            member.last()->isCompleteMestick = local->query->value(8).toBool();
            member.last()->type = local->query->value(0).toInt();
            if (member.last()->type%2 == 1)
                member.last()->image = new QImage(local->login + "/"
                                                  + local->query->value(2).toString());
            if (((int)(member.last()->type/2))%2 == 1)
                member.last()->groupList = local->query->value(3).toInt();
            if (((int)(member.last()->type/4))%2 == 1)
                member.last()->text = local->query->value(4).toString();
            if (((int)(member.last()->type/8))%2 == 1)
                member.last()->timeEnd = local->query->value(5).toDateTime();
            qDebug() << member.last()->timeEnd << member.last()->timeStart;
        }

        for (int i = 0; i < member.length(); i++)
            if (member.at(i)->groupList != -1){
                bool isConnect = local->query->exec("SELECT title, timeEnd, isComplete, id "
                                   "FROM " + local->login + "List WHERE "
                                   "\"group\" = '" + QString::number(member.at(i)->groupList)
                                   + "';");
                qDebug() << "SELECT title, timeEnd, isComplete, id "
                            "FROM " + local->login + "List WHERE "
                            "\"group\" = '" + QString::number(member.at(i)->groupList)
                            + "';" << isConnect;
                while (local->query->next()){
                    member.at(i)->stickList.push_back(new StickList(
                                                          local->query->value(0).toString(),
                                                          local->query->value(1).toDateTime(),
                                                          local->query->value(2).toBool()));
                    member.at(i)->stickList.last()->id =  local->query->value(3).toInt();
                }
            }
    }
        break;
    case 1:
        break;
    case 2:
        break;
    case 3:
        break;
    default: ;
    }


    //0 - Все записи
    //1 - Записи в очереди
    //2 - Записи по времени
    //3 - Не временные записи

    //
    if (type == -1)
        emit signalSetImmediateMemory(member);
    else
        emit signalSetListNotice(member);
}

//авторизация
void ModelManager::slotGetLogin(QString login, QString password)
{
    qDebug() << "ModelManager::slotGetLogin" << login << password << server->isConnecting;
    if (server->isConnecting){
        qDebug() << "slotGetLogin modelmanager.cpp if";
        server->sendToServerLogin(login, password);
    }
    else {
        qDebug() << "slotGetLogin modelmanager.cpp else";
        QStringList ret = local->loginFunc(login, password);
        qDebug() << ret;
        emit signalSetLogin(ret.at(0), ret.at(1), ret.at(2));
    }
}

//выход с учетной записи
void ModelManager::slotGetLogout()
{
    local->logout();
}

void ModelManager::slotGetNotice(){}

//регистрация
void ModelManager::slotGetRegistration(QStringList regInfo)
{
    qDebug() << "slotGetReg modelmanager.cpp";
    qDebug() << regInfo << server->isConnecting;
    if (server->isConnecting){
        server->sendToServerReg(regInfo);
    }
    else
        emit signalSetRegistration(tr("Отсутствует соединение с сервером"));
}

void ModelManager::slotGetRemoveSticker(){}

void ModelManager::slotGetSynchronize(){}

//отправка результат смены пароля
void ModelManager::slotSetChangePass(QStringList result)
{
    if (result.at(0) != "")
        local->query->exec(result.at(0));
    emit signalSetChangePassword(result.at(1));
}

//отправка результат авторизации
void ModelManager::slotSetLogin(QStringList list)
{
    qDebug() << "slotSetLogin modelmanager.cpp";
    qDebug() << list;
    emit signalSetLogin(list.at(0), list.at(1), list.at(2));
    if (list.at(2) == ""){
        local->query->exec("SELECT `id` FROM Users WHERE `login` = '"
                           + list.at(1) + "';");
        local->query->next();
        if (local->query->isNull(0)){
            local->addAccountToLocal(list.at(3), list.at(1));
        }
        local->query->exec(list.at(4));
        local->query->exec("UPDATE Users SET isLogin = 0 WHERE login = '"
                       + list.at(1) +"';");
    }
}

//отправка результата регистрации
void ModelManager::slotSetReg(QStringList list)
{
    qDebug() << "slotSetReg modelmanager.cpp";
    qDebug() << list;
    if (list.at(0) == tr("successful")){
        local->addAccountToLocal(list.at(1), list.at(2));
    }
    emit signalSetRegistration(list.at(0));
}

//обновления статуса соединения с сервером
void ModelManager::slotSetNetworkStatus(bool isConnect, int ping)
{
    emit signalSetNetworkStatus(isConnect, ping);
}

//обновление информации по времени заметок
void ModelManager::slotGetImmediateMemory()
{
    int shiftTime = 100;
    qDebug() << "sloyfetimma modelm.cpp";
    QTimer::singleShot(shiftTime*1000, this, SLOT(slotGetImmediateMemory()));
    slotGetListNotice(-1, QStringList{"", ""});
}
