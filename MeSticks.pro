CONFIG += c++11
QT += sql widgets network

SOURCES += \
    main.cpp \
    Controler/mainctrl.cpp \
    Controler/controlstatichelp.cpp \
    Model/modelmanager.cpp \
    Model/localmanager.cpp \
    Model/servermanager.cpp \
    Model/sticklist.cpp \
    Model/memberstick.cpp \
    View/MainInterface/widgetmanager.cpp \
    View/MainInterface/sidebridge.cpp \
    View/MainInterface/upwidget.cpp \
    View/MainInterface/trayiconwidget.cpp \
    View/MainInterface/styleconfig.cpp \
    View/LeftSide/registration.cpp \
    View/LeftSide/login.cpp \
    View/LeftSide/editprofile.cpp \
    View/RightSide/settings.cpp \
    View/RightSide/Secondary/styleprototype.cpp \
    View/RightSide/about.cpp \
    View/RightSide/editsticker.cpp \
    View/RightSide/Secondary/sticklabel.cpp \
    View/StickWidget/stickcheckbox.cpp \
    View/StickWidget/stickscrollarea.cpp \
    View/RightSide/rightsidewidget.cpp \
    View/LeftSide/leftsidemenu.cpp \
    View/RightSide/viewstickerR.cpp \
    View/StickWidget/stickscrolllabel.cpp

HEADERS += \
    Controler/mainctrl.h \
    Controler/controlstatichelp.h \
    Model/modelmanager.h \
    Model/localmanager.h \
    Model/servermanager.h \
    Model/sticklist.h \
    Model/memberstick.h \
    View/MainInterface/widgetmanager.h \
    View/MainInterface/sidebridge.h \
    View/MainInterface/upwidget.h \
    View/MainInterface/trayiconwidget.h \
    View/MainInterface/styleconfig.h \
    View/LeftSide/registration.h \
    View/LeftSide/login.h \
    View/LeftSide/editprofile.h \
    View/RightSide/settings.h \
    View/RightSide/Secondary/styleprototype.h \
    View/RightSide/about.h \
    View/RightSide/editsticker.h \
    View/RightSide/Secondary/sticklabel.h \
    View/StickWidget/stickcheckbox.h \
    View/StickWidget/stickscrollarea.h \
    View/RightSide/rightsidewidget.h \
    View/LeftSide/leftsidemenu.h \
    View/RightSide/viewstickerR.h \
    View/StickWidget/stickscrolllabel.h

TRANSLATIONS += translate_ru.ts

RESOURCES += \
    images.qrc \
    qss.qrc
