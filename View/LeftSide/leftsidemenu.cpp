#include "leftsidemenu.h"

LeftSideMenu::LeftSideMenu()
{
    //создаем список названий кнопок и инициалищируем компоновщик
    lastSelect = nullptr;
    nameLeft << tr("Регистрация")
             << tr("Вход")
             << tr("Сменить пароль")
             << tr("Выход")
             << tr("Добавить записи")
             << tr("Просмотр записей")
             << tr("Настройки")
             << tr("Про программу");
    vBoxLeft = new QVBoxLayout;
    vBoxLeft->setMargin(0);
    vBoxLeft->setSpacing(1);

    fake = new QLabel;

    //создаем кнопки меню
    for (int i = 0; i < nameLeft.length(); i++){
        prototypeLeft.push_back(new QPushButton(nameLeft.at(i)));
        prototypeLeft.last()->setFlat(true);
        prototypeLeft.last()->setCheckable(true);
        prototypeLeft.last()->setFixedHeight(22);
        prototypeLeft.last()->setMinimumWidth(120);
        prototypeLeft.last()->setFocusPolicy(Qt::NoFocus);
        QObject::connect(prototypeLeft.last(), SIGNAL(clicked()),
                         this, SLOT(slotClickButton()));
    }

    registration = new Registration;
    login = new Login;
    editProfile = new EditProfile;

    //устанавливаем кнопки и виджеты в компоновщик
    vBoxLeft->addWidget(prototypeLeft.at(4));
    vBoxLeft->addWidget(prototypeLeft.at(5));
    vBoxLeft->addWidget(prototypeLeft.at(6));
    vBoxLeft->addWidget(prototypeLeft.at(7));

    vBoxLeft->addWidget(fake);
    vBoxLeft->addWidget(registration);
    vBoxLeft->addWidget(login);
    vBoxLeft->addWidget(editProfile);
    vBoxLeft->addWidget(prototypeLeft.at(0));
    vBoxLeft->addWidget(prototypeLeft.at(1));
    vBoxLeft->addWidget(prototypeLeft.at(2));
    vBoxLeft->addWidget(prototypeLeft.at(3));

    slotAllHideLeft();
    this->setLayout(vBoxLeft);
    this->setMinimumHeight(550);
    this->setFixedWidth(200);

    QObject::connect(registration,    SIGNAL(signalBackToMenu()),
                     this,            SLOT  (slotAllHideLeft()));

    QObject::connect(login,           SIGNAL(signalBackToMenu()),
                     this,            SLOT  (slotAllHideLeft()));

    QObject::connect(editProfile,     SIGNAL(signalBackToMenu()),
                     this,            SLOT  (slotAllHideLeft()));
}

//все прячем
void LeftSideMenu::slotAllHideLeft()
{
#ifdef Q_OS_ANDROID
    //settingLeft, leftViewSticker
    for (int i = 0; i < 2; i++){
        if (prototypeLeft.at(i)->isVisible())
            prototypeLeft.at(i)->setVisible(false);
        else if (! isAvtorizate)
            prototypeLeft.at(i)->setVisible(true);
    }
    for (int i = 2; i < 4; i++){
        if (prototypeLeft.at(i)->isVisible())
            prototypeLeft.at(i)->setVisible(false);
        else if (isAvtorizate)
            prototypeLeft.at(i)->setVisible(true);
    }
#endif
    fake->setVisible(true);
    login->setVisible(false);
    login->cleanEdits();
    editProfile->setVisible(false);
    editProfile->cleanEdits();
    registration->setVisible(false);
    registration->cleanEdits();
    for (int i = 0; i < prototypeLeft.length(); i++)
        prototypeLeft.at(i)->setChecked(false);
}

//отвечает за реакцию на нажатие какогото пункта меню
void LeftSideMenu::slotClickButton()
{
    lastSelect = (QPushButton*)sender();

    //registration
    if (prototypeLeft.at(0) == lastSelect){
        if (registration->isVisible()){
            registration->slotBackToMenu();
        }
        else {
            slotAllHideLeft();
            fake->setVisible(false);
            registration->setVisible(true);
        }
    }

    //login
    else if (prototypeLeft.at(1) == lastSelect){
        if (login->isVisible()){
            login->slotBackToMenu();
        }
        else {
            slotAllHideLeft();
            fake->setVisible(false);
            login->setVisible(true);
        }
    }

    //edit profile
    else if (prototypeLeft.at(2) == lastSelect){
        if (editProfile->isVisible()){
            editProfile->slotBackToMenu();
        }
        else {
            slotAllHideLeft();
            fake->setVisible(false);
            editProfile->setVisible(true);
        }
    }

    //logout
    else if (prototypeLeft.at(3) == lastSelect){
        emit signalLogout();
    }

    //add Sticker
    else if (prototypeLeft.at(4) == lastSelect){
        slotAllHideLeft();
        lastSelect->setChecked(true);
#ifdef Q_OS_ANDROID
        this->setVisible(false);
#endif
        emit signalAddNewStick();
    }

    //view Sticker
    else if (prototypeLeft.at(5) == lastSelect){
        if (! prototypeLeft.at(5)->isChecked()){
            fake->setVisible(true);
            slotSetRightWidget(0);
        }
        else {
            slotAllHideLeft();
#ifdef Q_OS_ANDROID
            fake->setVisible(false);
#endif
            slotSetRightWidget(2);
            lastSelect->setChecked(true);
        }
    }

    //settings
    else if (prototypeLeft.at(6) == lastSelect){
        if (! prototypeLeft.at(6)->isChecked()){
            fake->setVisible(true);
            slotSetRightWidget(0);
        }
        else {
            slotAllHideLeft();
            lastSelect->setChecked(true);
            slotSetRightWidget(3);
        }
    }

    //about
    else if (prototypeLeft.at(7) == lastSelect){
        slotAllHideLeft();
        lastSelect->setChecked(true);
        slotSetRightWidget(0);
    }
}

///меняет режимы отображения кнопок
///если авторизировано, тогда отображаеться выход и смена пароля
///если не авторищировано - войти и регистрация
void LeftSideMenu::loginMod(bool isLogin)
{
    if (isLogin){
        prototypeLeft.at(0)->setVisible(false);
        prototypeLeft.at(1)->setVisible(false);
        prototypeLeft.at(2)->setVisible(true);
        prototypeLeft.at(3)->setVisible(true);
    }
    else {
        prototypeLeft.at(0)->setVisible(true);
        prototypeLeft.at(1)->setVisible(true);
        prototypeLeft.at(2)->setVisible(false);
        prototypeLeft.at(3)->setVisible(false);
    }
    isAvtorizate = isLogin;
}

//вызов сигнала установить виджет в фокус
void LeftSideMenu::slotSetRightWidget(int i)
{
#ifdef Q_OS_ANDROID
    this->setVisible(false);
#endif
    emit signalSetRightWidget(i);
}
