#ifndef EDITPROFILE_H
#define EDITPROFILE_H

#include <Controler/controlstatichelp.h>
#include <View/StickWidget/stickcheckbox.h>

#include <QtCore/QDebug>
#include <QtGui/QResizeEvent>

#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>

class EditProfile : public QLabel
{
    Q_OBJECT
public:
    EditProfile();
    QLabel *fakeLabel;
    QLabel *emptyLabel;
    QLabel *oldPasswordLabel;
    QLabel *passwordLabel;
    QLabel *repeatPasswordLabel;
    QLabel *errorMsg;
    QLabel *errorLabel;
    QLineEdit *oldPasswordEdit;
    QLineEdit *passwordEdit;
    QLineEdit *repeatPasswordEdit;
    QPushButton *backButton;
    QPushButton *acceptButton;
    QHBoxLayout *backLayout;
    QVBoxLayout *vBox;
    StickCheckBox *switchShowPass;

    void resizeEvent(QResizeEvent *event);
    void cleanEdits();

public slots:
    void slotClickToAccept();
    void slotSwitchPasswordVisible();
    void slotBackToMenu();

signals:
    void signalBackToMenu();
    void signalEditProfile(QStringList);
};

#endif // EDITPROFILE_H
