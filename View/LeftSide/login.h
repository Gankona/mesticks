#ifndef LOGIN_H
#define LOGIN_H

#include <Controler/controlstatichelp.h>
#include <View/StickWidget/stickcheckbox.h>

#include <QtCore/QDebug>
#include <QtGui/QResizeEvent>

#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>

class Login : public QLabel
{
    Q_OBJECT
public:
    Login();
    QLabel *fakeLabel;
    QLabel *emptyLabel;
    QLabel *nameLabel;
    QLabel *passwordLabel;
    QLabel *errorMsg;
    QLabel *errorLabel;
    QLineEdit *nameEdit;
    QLineEdit *passwordEdit;
    QPushButton *backButton;
    QPushButton *acceptButton;
    QHBoxLayout *backLayout;
    QVBoxLayout *vBox;
    StickCheckBox *switchShowPass;

    void resizeEvent(QResizeEvent *event);
    void cleanEdits();

public slots:
    void slotClickToAccept();
    void slotSwitchPasswordVisible();
    void slotBackToMenu();

signals:
    void signalBackToMenu();
    void signalLogin(QString, QString);
};

#endif // LOGIN_H
