#include "login.h"

Login::Login()
{
    //создание интерфейса
    backLayout = new QHBoxLayout;
    backButton = new QPushButton("↩");
    backButton->setFocusPolicy(Qt::NoFocus);
    emptyLabel = new QLabel;
    backLayout->addWidget(backButton);
    backLayout->addWidget(emptyLabel);

    fakeLabel = new QLabel;
    errorMsg = new QLabel;

    nameLabel = new QLabel(tr("Логин"));
    passwordLabel = new QLabel(tr("Пароль"));
    nameEdit = new QLineEdit;
    passwordEdit = new QLineEdit;
    passwordEdit->setEchoMode(QLineEdit::Password);

    switchShowPass = new StickCheckBox(" " + tr("Показывать пароль"), false);
    switchShowPass->setOrientalLeftToRight();

    acceptButton = new QPushButton(tr("Войти"));
    acceptButton->setFocusPolicy(Qt::NoFocus);
    errorLabel = new QLabel;
    errorLabel->setWordWrap(true);

    vBox = new QVBoxLayout;
    vBox->setMargin(10);
    vBox->setSpacing(5);
    vBox->addLayout(backLayout);
    vBox->addWidget(fakeLabel);
    vBox->addWidget(errorMsg);
    vBox->addWidget(nameLabel);
    vBox->addWidget(nameEdit);
    vBox->addWidget(switchShowPass);
    vBox->addWidget(passwordLabel);
    vBox->addWidget(passwordEdit);
    vBox->addWidget(acceptButton);
    vBox->addWidget(errorLabel);

    this->setLayout(vBox);

    QObject::connect(acceptButton, SIGNAL(clicked()), this, SLOT(slotClickToAccept()));
    QObject::connect(switchShowPass, SIGNAL(signalSwitchCheck()), this, SLOT(slotSwitchPasswordVisible()));
    QObject::connect(backButton,   SIGNAL(clicked()), this, SLOT(slotBackToMenu()));
}

//отправляем сигнал возврата в меню
void Login::slotBackToMenu()
{
    emit signalBackToMenu();
}

//очитска полей ввода
void Login::cleanEdits()
{
    nameEdit->clear();
    passwordEdit->clear();
    passwordEdit->setEchoMode(QLineEdit::Password);
    switchShowPass->setDefault();
    errorLabel->clear();
    errorMsg->clear();
}

//вызываеться при нажатии кнопки войти
void Login::slotClickToAccept()
{
    bool isAllRight = true;

    //проверка на коректность поля имя
    QString test = ControlStaticHelp::testNamePole(nameEdit->text(),
                                                 tr("Имя"));
    if (test != "correct"){
        isAllRight = false;
        errorLabel->setText(test);
    }

    //проверка на коректность поля пароль
    test = ControlStaticHelp::testNamePole(passwordEdit->text(),
                                                 tr("Пароль"));
    if (test != "correct"){
        isAllRight = false;
        errorLabel->setText(test);
    }

    //если все заполнено, тогда запрос на авторизацию
    if (isAllRight){
        errorLabel->setText(tr("Поля заполнены коректно."));
        qDebug() << "1 step login";
        emit signalLogin(nameEdit->text(), passwordEdit->text());
    }
}

//смена режима отобразить/скрыть пароль за звездочками
void Login::slotSwitchPasswordVisible()
{
    if (switchShowPass->isChoose)
        passwordEdit->setEchoMode(QLineEdit::Normal);
    else
        passwordEdit->setEchoMode(QLineEdit::Password);
}

//изменение размеров компонентов
void Login::resizeEvent(QResizeEvent *event)
{
#ifdef Q_OS_ANDROID
    fakeLabel->setVisible(false);
    int h = event->size().height()/8;
#else
    int h = 30;
#endif
    acceptButton->setFixedHeight   (h);
    backButton->setFixedSize       (h, h);
    nameEdit->setFixedHeight       (0.75 * h);
    passwordEdit->setFixedHeight   (0.75 * h);
    switchShowPass->setFixedHeight (0.75 * h);
}
