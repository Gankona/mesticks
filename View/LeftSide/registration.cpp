#include "registration.h"

Registration::Registration()
{
    //создания интерефейса для регистрации
    listLabelName.clear();
    listLabel.clear();
    listEdit.clear();

    backButton = new QPushButton("↩", this);
    backButton->setFocusPolicy(Qt::NoFocus);

    listLabelName << tr("Имя")
                  << tr("Никнейм")
                  << tr("e-mail")
                  << tr("Пароль")
                  << tr("Повторите пароль")
                  << tr("Зарегестрировать");

    switchShowPass = new StickCheckBox(" " + tr("Показывать пароль"), false);
    switchShowPass->setParent(this);
    switchShowPass->setOrientalLeftToRight();

    for (int i = 0; i < listLabelName.length()-1; i++){
        listLabel.push_back(new QLabel(listLabelName.at(i), this));
        listEdit.push_back(new QLineEdit(this));
    }
    listEdit.at(listEdit.length()-1)->setEchoMode(QLineEdit::Password);
    listEdit.at(listEdit.length()-2)->setEchoMode(QLineEdit::Password);

    registButton = new QPushButton(listLabelName.last(), this);
    registButton->setFocusPolicy(Qt::NoFocus);
    errorLabel = new QLabel(this);
    errorLabel->setWordWrap(true);

    QObject::connect(switchShowPass, SIGNAL(signalSwitchCheck()),
                     this, SLOT(slotSwitchShowPassword()));
    QObject::connect(backButton, SIGNAL(clicked()),
                     this, SLOT(slotBackToMenu()));
    QObject::connect(registButton, SIGNAL(clicked()),
                     this, SLOT(slotRegistered()));
}

//смена режима отобразить/скрыть пароль за звездочками
void Registration::slotSwitchShowPassword()
{
    if (switchShowPass->isChoose){
        listEdit.at(listEdit.length()-1)->setEchoMode(QLineEdit::Normal);
        listEdit.at(listEdit.length()-2)->setEchoMode(QLineEdit::Normal);
    }
    else {
        listEdit.at(listEdit.length()-1)->setEchoMode(QLineEdit::Password);
        listEdit.at(listEdit.length()-2)->setEchoMode(QLineEdit::Password);
    }
}

//синал возврата назад в меню
void Registration::slotBackToMenu()
{
    emit signalBackToMenu();
}

//очистка полей ввода
void Registration::cleanEdits()
{
    for (int i = 0; i < listEdit.length(); i++)
        listEdit.at(i)->clear();
    switchShowPass->setDefault();
    listEdit.at(listEdit.length()-1)->setEchoMode(QLineEdit::Password);
    listEdit.at(listEdit.length()-2)->setEchoMode(QLineEdit::Password);
    errorLabel->clear();
}

//проверка на правильность веденных данных
//если все правильно тогда отправляем запрос на регистрацию
void Registration::slotRegistered()
{
    //проверка на совпадение паролей
    bool isAllRight = true;
    if (listEdit.at(listEdit.length()-2)->text() != listEdit.at(listEdit.length()-1)->text()){
        isAllRight = false;
        errorLabel->setText(tr("*пароли не совпадают"));
    }
    //проверка полей ввода, что бы там были только буквы и цифры
    for (int i = 0; (i < listEdit.length()) && (isAllRight); i++){
        QString test = ControlStaticHelp::testNamePole(listEdit.at(i)->text(),
                                                       listLabelName.at(i));
        if (test != "correct"){
            isAllRight = false;
            errorLabel->setText(test);
        }
    }

    //если все заполнено тогда отправляем запрос на регистрацию
    if (isAllRight){
        errorLabel->setText(tr("Поля заполнены коректно."));
        QStringList str;
        str.clear();
        for (int i = 0; i < listEdit.length()-1; i++)
            str << listEdit.at(i)->text();
        emit signalRegistered(str);
    }
}

//изменение размеров компонентов
void Registration::resizeEvent(QResizeEvent *event)
{
    int w = event->size().width()  - 20;
    int h = event->size().height() - 20;
    for (int i = 0; i < listEdit.length(); i++){
        if (i > 2){
            setSizeAndMove(listEdit.at(i),  QPoint(10, h*(i*2+4)/15), QSize(w, h/16));
            setSizeAndMove(listLabel.at(i), QPoint(10, h*(i*2+3)/15), QSize(w, h/16));
        }
        else {
            setSizeAndMove(listEdit.at(i),  QPoint(10, h*(i*2+3)/15), QSize(w, h/16));
            setSizeAndMove(listLabel.at(i), QPoint(10, h*(i*2+2)/15), QSize(w, h/16));
        }
    }
    setSizeAndMove(backButton,     QPoint(10, 10     ), QSize(h/15, h/15));
    setSizeAndMove(switchShowPass, QPoint(10, h*8 /15), QSize(w, h/16));
    setSizeAndMove(registButton,   QPoint(10, h*13/15), QSize(w, h/16));
    setSizeAndMove(errorLabel,     QPoint(10, h*14/15), QSize(w, h/16));
}

//изменение размеров и положения переданого виджета
void Registration::setSizeAndMove(QWidget *widget, QPoint pos, QSize size)
{
    widget->setFixedSize(size);
    widget->move(pos);
}
