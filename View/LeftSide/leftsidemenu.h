#ifndef LEFTSIDELABEL_H
#define LEFTSIDELABEL_H

#include <View/LeftSide/login.h>
#include <View/LeftSide/editprofile.h>
#include <View/LeftSide/registration.h>

#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>

class LeftSideMenu : public QLabel
{
    Q_OBJECT
public:
    LeftSideMenu();
    Login *login;
    EditProfile *editProfile;
    Registration *registration;

    QLabel *fake;
    QStringList nameLeft;
    QVBoxLayout *vBoxLeft;
    QPushButton *lastSelect;
    QList <QPushButton*> prototypeLeft;

    bool isAvtorizate;

    void loginMod(bool isLogin);

public slots:
    void slotAllHideLeft();
    void slotClickButton();
    void slotSetRightWidget(int i);

signals:
    void signalAddNewStick();
    void signalLogout();
    void signalSetRightWidget(int);
};

#endif // LEFTSIDELABEL_H
