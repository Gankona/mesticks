#include "editprofile.h"

EditProfile::EditProfile()
{
    //создание интерфейса
    backLayout = new QHBoxLayout;
    backButton = new QPushButton("↩");
    backButton->setFocusPolicy(Qt::NoFocus);
    emptyLabel = new QLabel;
    backLayout->addWidget(backButton);
    backLayout->addWidget(emptyLabel);

    fakeLabel = new QLabel;
    errorMsg = new QLabel;

    oldPasswordLabel = new QLabel(tr("Старый пароль"));
    passwordLabel = new QLabel(tr("Новый пароль"));
    repeatPasswordLabel = new QLabel(tr("Повторить"));
    oldPasswordEdit = new QLineEdit;
    oldPasswordEdit->setEchoMode(QLineEdit::Password);//
    passwordEdit = new QLineEdit;
    passwordEdit->setEchoMode(QLineEdit::Password);
    repeatPasswordEdit = new QLineEdit;
    repeatPasswordEdit->setEchoMode(QLineEdit::Password);

    //
    switchShowPass = new StickCheckBox(" " + tr("Показывать пароль"), false);
    switchShowPass->setOrientalLeftToRight();

    acceptButton = new QPushButton(tr("Войти"));
    acceptButton->setFocusPolicy(Qt::NoFocus);
    errorLabel = new QLabel;
    errorLabel->setWordWrap(true);//

    //
    vBox = new QVBoxLayout;
    vBox->setMargin(10);
    vBox->setSpacing(5);
    vBox->addLayout(backLayout);
    vBox->addWidget(fakeLabel);
    vBox->addWidget(errorMsg);
    vBox->addWidget(switchShowPass);
    vBox->addWidget(oldPasswordLabel);
    vBox->addWidget(oldPasswordEdit);
    vBox->addWidget(passwordLabel);
    vBox->addWidget(passwordEdit);
    vBox->addWidget(repeatPasswordLabel);
    vBox->addWidget(repeatPasswordEdit);
    vBox->addWidget(acceptButton);
    vBox->addWidget(errorLabel);

    this->setLayout(vBox);

    QObject::connect(acceptButton, SIGNAL(clicked()), this, SLOT(slotClickToAccept()));
    QObject::connect(switchShowPass, SIGNAL(signalSwitchCheck()), this, SLOT(slotSwitchPasswordVisible()));
    QObject::connect(backButton,   SIGNAL(clicked()), this, SLOT(slotBackToMenu()));

#ifdef Q_OS_ANDROID
    fakeLabel->setVisible(false);
#endif
}

//отправляем сигнал возврата в меню
void EditProfile::slotBackToMenu(){
    emit signalBackToMenu();
}

//очистка полей ввода
void EditProfile::cleanEdits()
{
    oldPasswordEdit->clear();
    passwordEdit->clear();
    repeatPasswordEdit->clear();
    oldPasswordEdit->setEchoMode(QLineEdit::Password);
    passwordEdit->setEchoMode(QLineEdit::Password);
    repeatPasswordEdit->setEchoMode(QLineEdit::Password);
    switchShowPass->setDefault();
    errorLabel->clear();
    errorMsg->clear();
}

//вызываеться при нажатии кнопки принять
void EditProfile::slotClickToAccept()
{
    bool isAllRight = true;

    //проверка паролей на совпадение новых паролей
    if (passwordEdit->text() == repeatPasswordEdit->text()){
        isAllRight = false;
        errorLabel->setText(tr("*новый пароль при проверке пароля не совпадает"));
    }

    //проверка коректности введения старого пароля
    QString test = ControlStaticHelp::testNamePole(oldPasswordEdit->text(),
                                                 tr("Старый пароль"));
    if (test != "correct"){
        isAllRight = false;
        errorLabel->setText(test);
    }

    //проверка введения нового пароля
    test = ControlStaticHelp::testNamePole(passwordEdit->text(),
                                                 tr("Новый пароль"));
    if (test != "correct"){
        isAllRight = false;
        errorLabel->setText(test);
    }

    //проверка введения нового пароля второй раз
    test = ControlStaticHelp::testNamePole(repeatPasswordEdit->text(),
                                                 tr("Подтверждение"));
    if (test != "correct"){
        isAllRight = false;
        errorLabel->setText(test);
    }

    //если все хорошо, тогда отправляем запрос на изменение пароля
    if (isAllRight){
        errorLabel->setText(tr("Поля заполнены коректно."));
        emit signalEditProfile(QStringList{oldPasswordEdit->text(), passwordEdit->text()});
    }
}

//смена режима отобразить/скрыть пароль за звездочками
void EditProfile::slotSwitchPasswordVisible()
{
    if (switchShowPass->isChoose){
        passwordEdit->setEchoMode(QLineEdit::Normal);
        oldPasswordEdit->setEchoMode(QLineEdit::Normal);
        repeatPasswordEdit->setEchoMode(QLineEdit::Normal);
    }
    else {
        passwordEdit->setEchoMode(QLineEdit::Password);
        oldPasswordEdit->setEchoMode(QLineEdit::Password);
        repeatPasswordEdit->setEchoMode(QLineEdit::Password);
    }
}

//изменение размеров компонентов
void EditProfile::resizeEvent(QResizeEvent *event)
{
#ifdef Q_OS_ANDROID
    fakeLabel->setVisible(false);
    int h = event->size().height()/8;
#else
    int h = 30;
#endif
    acceptButton->setFixedHeight      (h);
    backButton->setFixedSize          (h, h);
    oldPasswordEdit->setFixedHeight   (0.75 * h);
    passwordEdit->setFixedHeight      (0.75 * h);
    switchShowPass->setFixedHeight    (0.75 * h);
    repeatPasswordEdit->setFixedHeight(0.75 * h);
}
