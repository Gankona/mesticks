#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <Controler/controlstatichelp.h>
#include <View/StickWidget/stickcheckbox.h>

#include <QtCore/QDebug>
#include <QtGui/QResizeEvent>

#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

class Registration : public QLabel
{
    Q_OBJECT
public:
    Registration();
    QLabel *fakeLabel;
    QLabel *errorLabel;
    QPushButton *backButton;
    QPushButton *registButton;
    QStringList listLabelName;
    QList <QLabel*> listLabel;
    QList <QLineEdit*> listEdit;
    StickCheckBox *switchShowPass;

    void cleanEdits();
    void resizeEvent(QResizeEvent *event);
    void setSizeAndMove(QWidget *widget, QPoint pos, QSize size);

public slots:
    void slotRegistered();
    void slotBackToMenu();
    void slotSwitchShowPassword();

signals:
    void signalBackToMenu();
    void signalRegistered(QStringList);
};

#endif // REGISTRATION_H
