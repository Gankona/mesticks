#ifndef STICKSCROLLLABEL_H
#define STICKSCROLLLABEL_H

#include <QtWidgets/QFrame>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>

#ifdef Q_OS_ANDROID
#include <QtGui/QMouseEvent>
#include <QtCore/QTimer>
#endif

class StickScrollLabel : public QFrame
{
    Q_OBJECT
public:
    StickScrollLabel();

#ifdef Q_OS_ANDROID
    bool isReleaseScroll;
    int speedScroll;
    int yKor;
    int currentCursorYPos;

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *event);

public slots:
    void slotSpeedScrollControl();
#endif
};

#endif // STICKSCROLLLABEL_H
