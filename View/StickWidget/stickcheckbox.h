#ifndef STICKCHECKBOX_H
#define STICKCHECKBOX_H

#include <QtCore/QDebug>
#include <QtCore/QTextCodec>
#include <QtGui/QResizeEvent>

#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>

class StickCheckBox : public QLabel
{
    Q_OBJECT
public:
    StickCheckBox(QString names,
                  bool choose);
    QLabel *title;
    QPushButton *checkButton;
    QTextCodec *codec;

    bool isChoose;
    bool isRightToLeft;

    void setDefault();
    void setOrientalRightToLeft();
    void setOrientalLeftToRight();
    void resizeEvent(QResizeEvent *event);

public slots:
    void slotClickToButton();

signals:
    void signalSwitchCheck();
};

#endif // STICKCHECKBOX_H
