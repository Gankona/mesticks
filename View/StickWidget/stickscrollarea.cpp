#include "stickscrollarea.h"
#include <QDebug>

//создаем обьект отловки скролинга
StickScrollArea::StickScrollArea() : qsetting("Gankona", "MeSticks")
{
    stickerBox = new StickScrollLabel;
}

//отлавлдиваем скролинг колеса мыши
void StickScrollArea::wheelEvent(QWheelEvent *e)
{
#ifndef Q_OS_ANDROID
    //движение вверх
    if (e->angleDelta().y() > 0){
        int newY = stickerBox->pos().y()
                //сдесь читаеться количество пикселей на которое смещаетьсяекран
                //за один этап
                + qsetting.value("Setting/ScrollStep", "100").toInt();
        if (newY >= 0)
            stickerBox->move(stickerBox->pos().x(), 0);
        else
            stickerBox->move(stickerBox->pos().x(), newY);
    }
    //движение вниз
    else {
        int newY = stickerBox->pos().y()
                - qsetting.value("Setting/ScrollStep", "100").toInt();
        if ((-1)*newY > stickerBox->height() - this->sizeHint().height())
            stickerBox->move(stickerBox->pos().x(),
                             (-1)*(stickerBox->height() - this->sizeHint().height()));
        else
            stickerBox->move(stickerBox->pos().x(), newY);
    }
#endif
}
