#include "stickcheckbox.h"

StickCheckBox::StickCheckBox(QString names, bool choose)
{
    //mean in rightside button, in leftside label, if true
    isRightToLeft = true;
    title = new QLabel(names, this);
    title->setWordWrap(true);
    title->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    codec = QTextCodec::codecForName("UTF-8");

    checkButton = new QPushButton(this);
    checkButton->setFocusPolicy(Qt::NoFocus);
    checkButton->heightForWidth(30);

    QObject::connect(checkButton, SIGNAL(clicked()),
                     this, SLOT(slotClickToButton()));

#ifdef Q_OS_ANDROID
    if (isChoose)
        checkButton->setIcon(QPixmap(":/images/Files/Images/yesIcon.png"));
    else
        checkButton->setIcon(QPixmap(":/images/Files/Images/noIcon.png"));
#else
    if (isChoose)
        checkButton->setText(codec->toUnicode("✓"));
    else
        checkButton->setText(codec->toUnicode("✗"));
#endif
    isChoose = choose;
}

//смена размещения кнопки относительно текста
//кнопка слева, текст справа
void StickCheckBox::setOrientalLeftToRight()
{
    isRightToLeft = false;
    checkButton->move(0, 0);
    title->move(checkButton->width(), 0);
    title->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
}

//смена размещения кнопки относительно текста
//кнопка справа, текст слева
void StickCheckBox::setOrientalRightToLeft()
{
    isRightToLeft = true;
    title->move(0, 0);
    checkButton->move(title->width(), 0);
    title->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
}

//отлавливаем нажатия на кнопку
void StickCheckBox::slotClickToButton()
{
#ifdef Q_OS_ANDROID
    if (isChoose)
        checkButton->setIcon(QPixmap(":/images/Files/Images/noIcon.png"));
    else
        checkButton->setIcon(QPixmap(":/images/Files/Images/yesIcon.png"));
#else
    if (isChoose)
        checkButton->setText(codec->toUnicode("✗"));
    else
        checkButton->setText(codec->toUnicode("✓"));
#endif
    isChoose = !isChoose;
    emit signalSwitchCheck();
}

//смена размеров виджета, при вызове перерисовки размера
void StickCheckBox::resizeEvent(QResizeEvent *event)
{
    title->setFixedHeight(event->size().height());
    title->setFixedWidth(event->size().width() - event->size().height());

    checkButton->setFixedSize(event->size().height(), event->size().height());
    checkButton->setIconSize(checkButton->size());

    if (isRightToLeft)
        setOrientalRightToLeft();
    else
        setOrientalLeftToRight();
}

//сброс названия и параметров до изначальных
void StickCheckBox::setDefault()
{
#ifdef Q_OS_ANDROID
    checkButton->setIcon(QPixmap(":/images/Files/Images/noIcon.png"));
#else
    checkButton->setText(codec->toUnicode("✗"));
#endif
    isChoose = false;
}
