#ifndef STICKSCROLLAREA_H
#define STICKSCROLLAREA_H

#include <View/StickWidget/stickscrolllabel.h>

#include <QtCore/QSettings>
#include <QtGui/QWheelEvent>
#include <QtWidgets/QScrollArea>

class StickScrollArea : public QScrollArea
{
public:
    StickScrollArea();
    QSettings qsetting;
    StickScrollLabel *stickerBox;

    void wheelEvent(QWheelEvent *e);
};

#endif // STICKSCROLLAREA_H
