#include "stickscrolllabel.h"
#include <QDebug>

StickScrollLabel::StickScrollLabel(){}

#ifdef Q_OS_ANDROID
//отлавливает нажатие мышью по программе
void StickScrollLabel::mousePressEvent(QMouseEvent *event)
{
    qDebug() << "press" << event;
    yKor = event->screenPos().y();
    QTimer::singleShot(40, this, SLOT(slotSpeedScrollControl()));
    isReleaseScroll = false;
    speedScroll = 0;
}

//отлавливает отпускание мыши после нажатия
void StickScrollLabel::mouseReleaseEvent(QMouseEvent *)
{
    isReleaseScroll = true;
}

//отлавливает перемещение мыши и извлекает координаты
void StickScrollLabel::mouseMoveEvent(QMouseEvent *event)
{
    qDebug() << "move" << event << event->screenPos();
    currentCursorYPos = event->screenPos().y();
    qDebug() << "move" << currentCursorYPos;
}

//создает движение данного виджета и контролирует снижение скорости
//в зависимоти от времени
void StickScrollLabel::slotSpeedScrollControl()
{
    qDebug() << "speed" << speedScroll << this->height();
    int shift =  5*speedScroll;
    qDebug() << "this position" << this->pos() << shift;
    this->move(this->pos().x(), this->pos().y() + shift);
    //контролирует что бы не перемещение не выходило за рамки размеров виджета
    if (this->pos().y() >= 10)
        this->move(this->pos().x(), 10);
    else if (this->pos().y() <      (-1)*(this->height() - QApplication::desktop()->height()))
        this->move(this->pos().x(), (-1)*(this->height() - QApplication::desktop()->height()));

    //снижение скорости и перемещение виджета
    if ( ! isReleaseScroll){
        QTimer::singleShot(10, this, SLOT(slotSpeedScrollControl()));
        speedScroll = currentCursorYPos - yKor;
        yKor = currentCursorYPos;
    }
    //контролирует скорость, если она ниже 1 пикселя то останавливаемся
    else if ((speedScroll > 1) || (speedScroll < -1)){
        QTimer::singleShot(10, this, SLOT(slotSpeedScrollControl()));
        speedScroll /= 2;
    }
}
#endif
