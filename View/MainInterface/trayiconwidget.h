#ifndef TRAYICONWIDGET_H
#define TRAYICONWIDGET_H

#include <Model/memberstick.h>

#include <QtCore/QTimer>
#include <QtCore/QDebug>
#include <QtCore/QDateTime>

#include <QtWidgets/QMenu>
#include <QtWidgets/QSystemTrayIcon>

class TrayIconWidget : public QSystemTrayIcon
{
    Q_OBJECT
public:
    TrayIconWidget();
    QMenu *trayMenu;
    QList<MemberStick *> memberList;

    void refreshList(QList<MemberStick *> member);

public slots:
    void slotSwitchVisible();
    void slotCloseApplication();
    void slotSetTrayMessage(MemberStick *stick);

signals:
    void signalSwitchVisible();
    void signalCloseApplication();
};

#endif // TRAYICONWIDGET_H
