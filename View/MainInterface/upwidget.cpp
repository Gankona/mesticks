#include "upwidget.h"
#include <QPainter>

UpWidget::UpWidget()
{
    //создаем интерфейс верхней части интерфейса
    leftSideButton = new QPushButton;
    leftSideButton->setFocusPolicy(Qt::NoFocus);
    centralSide = new QLabel;
    rightSide = new QLabel;
    rightSide->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    networkLabel = new QLabel;
    networkLabel->setScaledContents(true);
    networkLabel->setPixmap(QPixmap(":/images/Files/Images/serverNotFound.png"));
    networkLabel->setToolTip(tr("соединение отсутствует"));

    hBox = new QHBoxLayout;
    hBox->setMargin(0);
    hBox->setSpacing(0);
    hBox->addWidget(leftSideButton);

#ifdef Q_OS_ANDROID
    leftSideButton->setIcon(QPixmap(":/images/Files/Images/menu.png"));
    leftSideButton->setFixedSize(75, 75);
    leftSideButton->setIconSize(QSize(50, 50));
    networkLabel->setFixedSize(75, 75);
    centralSide->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    vBox = new QVBoxLayout;
    vBox->setSpacing(0);
    vBox->setMargin(0);
    vBox->addWidget(centralSide);
    vBox->addWidget(rightSide);
    hBox->addWidget(networkLabel);
    hBox->addLayout(vBox);
#else
    centralSide->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    leftSideButton->setText(/*codec->toUnicode*/("☰"));
    leftSideButton->setFixedSize(25, 25);
    leftSide = new QLabel(" Меню");
    leftSide->setFixedSize(175, 25);
    networkLabel->setFixedSize(25, 25);

    hBox->addWidget(leftSide);
    hBox->addWidget(networkLabel);
    hBox->addWidget(centralSide);
    hBox->addWidget(rightSide);
    this->setFixedHeight(25);
#endif

    this->setLayout(hBox);

    QObject::connect(leftSideButton, SIGNAL(clicked()), this, SLOT(slotClickToLeft()));

    QTimer::singleShot(1000, this, SLOT(slotClock()));
}

// скрыть/отобразить главное окно
void UpWidget::slotClickToLeft()
{
#ifndef Q_OS_ANDROID
    leftSide->setVisible( ! leftSide->isVisible());
#endif
    //выхов сигнала на смену видимости главного окна
    emit signalSwitchVisibleLeftSide();
}

//обновляем часы
void UpWidget::slotClock()
{
    QTimer::singleShot(1000, this, SLOT(slotClock()));
    rightSide->setText(QDateTime::currentDateTime().toString() + "   ");
}
