#include "styleconfig.h"

StyleConfig::StyleConfig()
        : qsetting("Gankona", "MeSticks")
{
    //загрузка информации стиля и создания прототипов стиля
    mapTheme.clear();
    mapTheme.insert(tr("greenTheme"),    ControlStaticHelp::readQss("greenTheme"));
    mapTheme.insert(tr("darkTheme"),     ControlStaticHelp::readQss("darkTheme"));
    mapTheme.insert(tr("blueTheme"),     ControlStaticHelp::readQss("blueTheme"));
    mapTheme.insert(tr("winterTheme"),   ControlStaticHelp::readQss("winterTheme"));
    mapTheme.insert(tr("kontrastTheme"), ControlStaticHelp::readQss("kontrastTheme"));
    mapTheme.insert(tr("brownTheme"),    ControlStaticHelp::readQss("brownTheme"));
}

//чтение стиля при старте программы, если первый запуск то старт с темной темы
void StyleConfig::startColorSchema()
{
    currentColorSchema = qsetting.value("/Setting/ColorSchema", "darkTheme").toString();
    setColorTheme(currentColorSchema, true);
}

//установка нового стиля
QMap <QString, QStringList> StyleConfig::setColorTheme(QString theme, bool isSet)
{
    if (isSet){
        qsetting.setValue("/Setting/ColorSchema", theme);
        emit signalSetColor(mapTheme[theme]);
    }
    QMap <QString, QStringList> ret;
    ret.insert(theme, mapTheme[theme]);
    return ret;
}
