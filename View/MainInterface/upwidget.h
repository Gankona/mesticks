#ifndef UPWIDGET_H
#define UPWIDGET_H

#include <QtWidgets/QLabel>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>

#include <QtCore/QTimer>
#include <QtCore/QDebug>
#include <QtCore/QDateTime>
#include <QtCore/QTextCodec>

#ifdef Q_OS_ANDROID
#include <QtWidgets/QVBoxLayout>
#endif

class UpWidget : public QLabel
{
    Q_OBJECT
public:
    UpWidget();
    QPushButton *leftSideButton;
    QLabel *leftSide;
    QLabel *rightSide;
    QLabel *centralSide;
    QLabel *networkLabel;
    QHBoxLayout *hBox;
    QTextCodec *codec;
#ifdef Q_OS_ANDROID
    QVBoxLayout *vBox;
#endif

public slots:
    void slotClickToLeft();
    void slotClock();

signals:
    void signalSwitchVisibleLeftSide();
};

#endif // UPWIDGET_H
