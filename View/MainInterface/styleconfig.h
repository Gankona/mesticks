#ifndef STYLECONFIG_H
#define STYLECONFIG_H

#include <Controler/controlstatichelp.h>

#include <QtCore/QDebug>
#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QString>
#include <QtCore/QStringList>

class StyleConfig : public QObject
{
    Q_OBJECT
public:
    StyleConfig();
    QSettings qsetting;
    QString currentColorSchema;
    QMap <QString, QStringList> mapTheme;

    void startColorSchema();

public slots:
    QMap<QString, QStringList> setColorTheme(QString theme, bool isSet);

signals:
    void signalSetColor(QStringList);
};

#endif // STYLECONFIG_H
