#include "trayiconwidget.h"

TrayIconWidget::TrayIconWidget()
{
    //создания контекстного меню
    trayMenu = new QMenu;
    trayMenu->addAction(tr("&показать/спрятать"),
                        this,
                        SLOT(slotSwitchVisible()));
    trayMenu->addAction(tr("&выход"),
                        this,
                        SLOT(slotCloseApplication()));

    //установка значка на трей
    this->setIcon(QPixmap(":/images/Files/Images/trayIcon.png"));
    this->setContextMenu(trayMenu);
}

//отправка сигнала на закрытие приложения
void TrayIconWidget::slotCloseApplication()
{
    emit signalCloseApplication();
}

//сигнал на смену видимости главного окна
void TrayIconWidget::slotSwitchVisible()
{
    emit signalSwitchVisible();
}

//обновляем список задач у которых скоро прийдет срок отобразиться
//старые удаляем, и записываем на их место новые что бы не было повторов
void TrayIconWidget::refreshList(QList<MemberStick *> member)
{
    qDebug() << "reafresgList";
    memberList.clear();
    memberList = member;

    foreach (MemberStick *stick, member){
        qDebug() << "getPush";
        QTimer::singleShot(QDateTime::currentDateTime().secsTo(stick->timeEnd),
                           this, SLOT(slotSetTrayMessage(stick)));
    }
}

//отображает сообщение про исход времени на выполнение заметки
void TrayIconWidget::slotSetTrayMessage(MemberStick *stick)
{
    this->showMessage(stick->title, stick->text);
}
