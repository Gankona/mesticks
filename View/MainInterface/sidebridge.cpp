#include "sidebridge.h"

SideBridge::SideBridge() : qsetting("Gankona", "MeSticks")
{
    //инициализация компонентов интерфейса и стилей
    style = new StyleConfig;
    up = new UpWidget;
    up->setParent(this);
    left = new LeftSideMenu;
    left->setParent(this);
    right = new RightSideWidget(style);
    right->setParent(this);

    //установка размеров интерефейса
    int height;
    int width;

#ifdef Q_OS_ANDROID
    height = QApplication::desktop()->height();
    width = QApplication::desktop()->width();
    minimumSizeWindow = QApplication::desktop()->size();
    right->setVisible(false);
    left->setVisible(true);
#else
    height = qsetting.value("/Setting/height", up->height() + left->minimumHeight()).toInt();
    width  = qsetting.value("/Setting/width", left->width() + right->minimumWidth()).toInt();
    minimumSizeWindow.setHeight(up->height() + left->minimumHeight());
    minimumSizeWindow.setWidth(left->width() + right->minimumWidth());
#endif

    this->setSize(QSize(width, height));

    QObject::connect(left, SIGNAL(signalSetRightWidget(int)), right, SLOT(slotSetWidgetFocus(int)));

    QObject::connect(style, SIGNAL(signalSetColor(QStringList)), this, SLOT(slotSetNewStyle(QStringList)));

    style->startColorSchema();
    QObject::connect(up, SIGNAL(signalSwitchVisibleLeftSide()), this, SLOT(slotSwitchVisibleLeft()));

    QObject::connect(left, SIGNAL(signalAddNewStick()), right, SLOT(slotAddNewStick()));
}

//отобразить или скрыть меню в главном окне
void SideBridge::slotSwitchVisibleLeft()
{
#ifdef Q_OS_ANDROID
    right->setVisible(left->isVisible());
#endif
    left->setVisible( ! left->isVisible());
    this->setSize(this->size());
}

//установка нового стиля приложению
void SideBridge::slotSetNewStyle(QStringList newStyle)
{
    if(newStyle.length() > 1){;
        left ->setStyleSheet(newStyle.at(0));
        up   ->setStyleSheet(newStyle.at(1));
        right->setStyleSheet(newStyle.at(2));
    }
}

//сохранения новых размеров в регистре,
//при этом они не должны быть меньше определенных размеров
void SideBridge::resizeEvent(QResizeEvent *e)
{
    QSize newSize = e->size();
#ifndef Q_OS_ANDROID
    if (newSize.height() < minimumSizeWindow.height())
        newSize.setHeight(minimumSizeWindow.height());
    if (newSize.width() < minimumSizeWindow.width())
        newSize.setWidth(minimumSizeWindow.width());
#endif
    this->setSize(newSize);
    qsetting.setValue("/Setting/height", newSize.height());
    qsetting.setValue("/Setting/width", newSize.width());
}

//смена размеров компонентов окна при смене размеров виджета
void SideBridge::setSize(QSize newSize)
{
    this->resize(newSize);

#ifdef Q_OS_ANDROID
    newSize.setHeight(newSize.height()-75);
    left->setFixedSize(newSize);
    left->move(0, 75);
    right->setFixedSize(newSize);
    right->move(0, 75);
    up->setFixedSize(newSize.width(), 75);
    up->move(0, 0);
    foreach (QPushButton *at, left->prototypeLeft)
        at->setFixedHeight(newSize.height()/10);
#else

    up->setFixedSize(newSize.width(), up->height());
    up->move(0, 0);

    right->setFixedHeight(newSize.height() - up->height());

    if (left->isVisible() || !this->isVisible()){
        left->setFixedHeight(newSize.height() - up->height());
        left->move(0, up->height());
        right->setFixedWidth(newSize.width() - left->width());
        right->move(left->width(), up->height());
    }
    else {
        right->setFixedWidth(newSize.width());
        right->move(0, up->height());
    }
#endif
}
