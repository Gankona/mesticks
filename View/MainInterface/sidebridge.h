#ifndef SIDEBRIDGE_H
#define SIDEBRIDGE_H

#include <View/LeftSide/leftsidemenu.h>
#include <View/RightSide/rightsidewidget.h>
#include <View/MainInterface/styleconfig.h>
#include <View/MainInterface/upwidget.h>

#include <QtCore/QDebug>
#include <QtCore/QSettings>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDesktopWidget>

class SideBridge : public QMainWindow
{
    Q_OBJECT
public:
    SideBridge();
    QSettings qsetting;

    UpWidget *up;
    StyleConfig *style;
    LeftSideMenu *left;
    RightSideWidget *right;
    QSize minimumSizeWindow;

    void resizeEvent(QResizeEvent *e);
    void setSize(QSize newSize);

public slots:
    void slotSetNewStyle(QStringList newStyle);
    void slotSwitchVisibleLeft();
};

#endif // SIDEBRIDGE_H
