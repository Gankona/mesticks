#ifndef WIDGETMANAGER_H
#define WIDGETMANAGER_H

#include <View/MainInterface/sidebridge.h>
#include <View/MainInterface/trayiconwidget.h>

#include <QtCore/QDateTime>
#include <QtCore/QTimer>
#include <QtCore/QDebug>
#include <QtCore/QTimer>
#include <QtGui/QImage>
#include <QtWidgets/QApplication>

class WidgetManager : public QObject
{
    Q_OBJECT
public:
    WidgetManager();
    SideBridge *widget;
    TrayIconWidget *tray;

    bool isLogin;

public slots:
    void slotCloseApplication();
    void slotSwitchVisible();

    void slotSetLogin(QString name, QString login, QString error);
    void slotSetRegistration(QString resultReg);
    void slotSetChangePassword(QString resultChange);

    void slotSetAddSticker(QString resultAddSticker);
    void slotSetSynchronize();
    void slotSetEditSticker();
    void slotSetListNotice(QList<MemberStick *> member);
    void slotSetNotice();
    void slotSetRemoveSticker();

    void slotGetLogin(QString login, QString password);
    void slotGetLogout();
    void slotGetChangePassword(QStringList changePass);
    void slotGetRegistration(QStringList regInfo);

    void slotGetAddSticker(MemberStick *sender);
    void slotGetSynchronize();
    void slotGetEditSticker(MemberStick *sender, bool isEdit);
    void slotGetListNotice(int type, QStringList parametr);
    void slotGetNotice();
    void slotGetRemoveSticker();

    void slotSetNetworkStatus(bool isConnect, int ping);
    void slotSetImmediateMemory(QList<MemberStick *> member);

signals:
    void signalGetChangePassword(QStringList);
    void signalGetLogin(QString, QString);
    void signalGetLogout();
    void signalGetRegistration(QStringList);

    void signalGetAddSticker(MemberStick*);
    void signalGetDoSynchronize();
    void signalGetEditSticker(MemberStick*, bool);
    void signalGetListNotice(int, QStringList);
    void signalGetNotice();
    void signalGetRemoveSticker();
};

#endif // WIDGETMANAGER_H
