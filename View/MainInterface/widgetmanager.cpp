#include "widgetmanager.h"

WidgetManager::WidgetManager()
{
    //создаем главное окно интерфейса приложения
    widget = new SideBridge;
    //создаем значок приложения в трее
    tray = new TrayIconWidget;

    //отлавливаем сигналы от трея
    QObject::connect(tray, SIGNAL(signalCloseApplication()),
                     this, SLOT  (slotCloseApplication()));

    QObject::connect(tray, SIGNAL(signalSwitchVisible()),
                     this, SLOT  (slotSwitchVisible()));

    //отлавливаем сигналы от главного окна
    QObject::connect(widget->left->registration, SIGNAL(signalRegistered(QStringList)),
                     this, SLOT(slotGetRegistration(QStringList)));

    QObject::connect(widget->left->login, SIGNAL(signalLogin(QString,QString)),
                     this, SLOT(slotGetLogin(QString,QString)));

    QObject::connect(widget->left, SIGNAL(signalLogout()),
                     this, SLOT(slotGetLogout()));

    QObject::connect(widget->left->editProfile, SIGNAL(signalEditProfile(QStringList)),
                     this, SLOT(slotGetChangePassword(QStringList)));

    QObject::connect(widget->right->editSticker, SIGNAL(signalCreateNewStick(MemberStick*)),
                     this, SLOT(slotGetAddSticker(MemberStick*)));

    QObject::connect(widget->right->editSticker, SIGNAL(signalEditStick(MemberStick*, bool)),
                     this, SLOT(slotGetEditSticker(MemberStick*, bool)));

    QObject::connect(widget->right->viewSticker, SIGNAL(signalGetEditSticker(MemberStick*, bool)),
                     this, SLOT(slotGetEditSticker(MemberStick*, bool)));

    QObject::connect(widget->right->viewSticker, SIGNAL(signalGetListNotice(int,QStringList)),
                     this, SLOT(slotGetListNotice(int,QStringList)));
}

//закрываем приложения
void WidgetManager::slotCloseApplication()
{
    delete tray;
    delete widget;
    QApplication::quit();
}

//добавляем новую заметку
void WidgetManager::slotGetAddSticker(MemberStick *sender)
{
    emit signalGetAddSticker(sender);
}

//отправить сигнал для смены пароля
void WidgetManager::slotGetChangePassword(QStringList changePass)
{
    emit signalGetChangePassword(changePass);
}

//отправляем сигнал для редактирования заметки
void WidgetManager::slotGetEditSticker(MemberStick *sender, bool isEdit)
{
    emit signalGetEditSticker(sender, isEdit);
}

//отправляем запрос выдачи списка заметок
void WidgetManager::slotGetListNotice(int type, QStringList parametr)
{
    emit signalGetListNotice(type, parametr);
}

//запрос на авторизацию
void WidgetManager::slotGetLogin(QString login, QString password)
{
    emit signalGetLogin(login, password);
}

//выходим с учетной записи
void WidgetManager::slotGetLogout()
{
    emit signalGetLogout();
    widget->left->loginMod(false);
    widget->up->centralSide->setText(" " + tr("Не авторизировано"));
    widget->up->centralSide->setText(tr(" [NoName] "));
    //widget->right->slotSetWidgetFocus(0);
}

//
void WidgetManager::slotGetNotice(){}

//отправляем сигнал регистрации
void WidgetManager::slotGetRegistration(QStringList regInfo)
{
    emit signalGetRegistration(regInfo);
}

//
void WidgetManager::slotGetRemoveSticker(){}

//
void WidgetManager::slotGetSynchronize(){}

//отображаем результат внесения данных в БД
void WidgetManager::slotSetAddSticker(QString resultAddSticker)
{
    widget->right->editSticker->errorMsgLabel->setText(resultAddSticker);
}

//отображаем результат смены пароля или ошибки
void WidgetManager::slotSetChangePassword(QString resultChange)
{
    widget->left->editProfile->errorLabel->setText(resultChange);
}

//
void WidgetManager::slotSetEditSticker(){}

//отправляем список заметок для отображения, что соответствуют запросу
void WidgetManager::slotSetListNotice(QList <MemberStick*> member)
{
    widget->right->viewSticker->setMemberStick(member);
}

//устанавливаем пароль или выдаем ошибки при попытке авторизироваться
void WidgetManager::slotSetLogin(QString name, QString login, QString error)
{
    qDebug() << "8 step login";
    qDebug() << "widgetmanager slotSetLogin";
    if (error == ""){
        widget->left->loginMod(true);
        widget->left->login->slotBackToMenu();
        widget->up->centralSide->setText(" " + login + " [ "
                                               + name + " ]");
    }
    else
        widget->left->login->errorLabel->setText("* " + error);
}

//
void WidgetManager::slotSetNotice(){}

//возвращаем результат регистрации или отображаем ошибки
void WidgetManager::slotSetRegistration(QString resultReg)
{
    qDebug() << "hi, i'm an answer from registration!";
    qDebug() << resultReg << "resultReg";
    if (resultReg == "successful")
        widget->left->registration->errorLabel->setText(tr("Регистрация прошла успешно, "
                                                  "войдите в учетную запись для начала пользования"));
    else
        widget->left->registration->errorLabel->setText("* " + resultReg);
}

//
void WidgetManager::slotSetRemoveSticker(){}

//
void WidgetManager::slotSetSynchronize(){}

//прячем/отображаем главное окно
void WidgetManager::slotSwitchVisible()
{
    widget->setVisible( ! widget->isVisible());
}

//устанвливаем информацию про соединение с сервером
void WidgetManager::slotSetNetworkStatus(bool isConnect, int ping)
{
    if (isConnect)
        widget->up->networkLabel->setPixmap(QPixmap(":/images/Files/Images/serverFound.png"));
    else
        widget->up->networkLabel->setPixmap(QPixmap(":/images/Files/Images/serverNotFound.png"));

    if (isConnect)
        widget->up->networkLabel->setToolTip(tr("соединение установлено, ping ")
                                             + QString::number(ping) + tr("ms"));
    else
        widget->up->networkLabel->setToolTip(tr("соединение отсутствует"));
}

//ищем заметки у которых в ближайшее время истечет срок действия
void WidgetManager::slotSetImmediateMemory(QList<MemberStick *> member)
{
    qDebug() << "slotsetImmediate wm.cpp" << member.length();
    QList<MemberStick *> memberSend;
    memberSend.clear();
    foreach(MemberStick *at, member){
        if ((((int)(at->type/8))%2 == 1)
                && (QDateTime::currentDateTime().secsTo(at->timeEnd) <= 100)
                && (QDateTime::currentDateTime().secsTo(at->timeEnd) >  0))
                memberSend.push_back(at);
        else
            delete at;
    }
    tray->refreshList(memberSend);
}
