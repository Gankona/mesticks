#include "viewstickerR.h"
#include <QtCore/QThread>

//виджет для просмотра заметок
ViewStickerR::ViewStickerR()
{
    //очистка списков и инициализация компоновщиков
    stickLabelList.clear();
    buttonLinkList.clear();
    typeShowList.clear();
    currentLink = 0;

    typeShowLayout = new QGridLayout;
    typeShowLayout->setMargin(2);
    typeShowLayout->setSpacing(2);

    vLinkBox = new QGridLayout;
    vLinkBox->setMargin(1);
    vLinkBox->setSpacing(2);

    vStickBox = new QVBoxLayout;
    vStickBox->setMargin(5);
    vStickBox->setSpacing(20);

    mainGrid = new QGridLayout;
    mainGrid->setMargin(1);
    mainGrid->setSpacing(1);

    //создание главных окон в виджете
    fake = new QLabel;
    fakeList = new QLabel;
    upLabel = new QLabel(tr("Просмотр напоминаний"));
    upLabel->setAlignment(Qt::AlignCenter);
    upLabel->setFixedHeight(30);

    upButton = new QPushButton((QChar)8896);

    upButton->setFocusPolicy(Qt::NoFocus);
    downButton = new QPushButton((QChar)8897);

    downButton->setFocusPolicy(Qt::NoFocus);
#ifdef Q_OS_ANDROID
    upButton->setFixedSize(60, 60);
    downButton->setFixedSize(60, 60);
#else
    upButton->setFixedSize(30, 30);
    downButton->setFixedSize(30, 30);
#endif

    //создание поля просмотра заметок и кнопка выбора выборки
    scrollArea = new StickScrollArea;
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea->stickerBox->setLayout(vStickBox);

    typeShowList.push_back(new QPushButton(tr("все")));
    typeShowList.push_back(new QPushButton(tr("на сутки")));
    typeShowList.push_back(new QPushButton(tr("архив")));

    typeShowLayout->addWidget(upLabel, 1, 1, 1, 3);
    //установка параметров кнопок
    int i = 1;
    foreach (QPushButton *b, typeShowList){
       typeShowLayout->addWidget(b, 2, i);
       i++;
       b->setFocusPolicy(Qt::NoFocus);
#ifdef Q_OS_ANDROID
       b->setFixedHeight(50);
#else
       b->setFixedHeight(20);
#endif
       QObject::connect(b, SIGNAL(clicked()), this, SLOT(slotGetListNotice()));
    }

    vLinkBox->addWidget(upButton, 1, 1);
    vLinkBox->addWidget(downButton, 1000, 1);
    vLinkBox->addWidget(fakeList, 999, 1);

    //mainGrid->addWidget(upLabel, 0, 0, 0, 2);
    mainGrid->addLayout(typeShowLayout, 1, 0, 1, 2);
    mainGrid->addLayout(vLinkBox, 2, 0);
    mainGrid->addWidget(scrollArea, 2, 1);
    mainGrid->addWidget(fake, 2, 2);

    this->setLayout(mainGrid);

    QTimer::singleShot(100, this, SLOT(slotTrekEvent()));

    QObject::connect(upButton,   SIGNAL(clicked()), this, SLOT(slotClickToLinkButton()));
    QObject::connect(downButton, SIGNAL(clicked()), this, SLOT(slotClickToLinkButton()));
}

//добавление заметок, эта функция вызываеться после того
//как был отослан запрос на выбор по какой то из выборки
void ViewStickerR::setMemberStick(QList<MemberStick *> member)
{
    cleanStickList();
    qDebug() << "start create Interface View Mesticks" << QTime::currentTime();
    qDebug() << "setMemberSticker RightViewSticker.cpp";
    qDebug() << member.length();
    for (int i = 0; i < member.length(); i++){
        stickLabelList.push_back(new StickLabel(member.at(i)));
        vStickBox->addWidget(stickLabelList.last());

        buttonLinkList.push_back(new QPushButton(QString::number(i+1)));
#ifdef Q_OS_ANDROID
        buttonLinkList.last()->setFixedSize(60, 60);
#else
        buttonLinkList.last()->setFixedSize(30, 30);
#endif
        buttonLinkList.last()->setCheckable(true);
        //buttonLinkList.last()->setStyleSheet(fonG);
        buttonLinkList.last()->setFocusPolicy(Qt::NoFocus);
        vLinkBox->addWidget(buttonLinkList.last(), i+2, 1);

        QObject::connect(buttonLinkList.last(), SIGNAL(clicked()),
                         this, SLOT(slotClickToLinkButton()));
        QObject::connect(stickLabelList.last(), SIGNAL(signalGetEdit(MemberStick*)),
                         this, SLOT(slotCatchCompleteClick(MemberStick*)));
        QObject::connect(stickLabelList.last(), SIGNAL(signalSetToEditMod(MemberStick*)),
                         this, SLOT(slotSetToEditMod(MemberStick*)));
    }
    qDebug() << "setMemberSticker 2 RightViewSticker.cpp";
    scrollArea->setWidget(scrollArea->stickerBox);
    maxLink = stickLabelList.length();
    qDebug() << "setMemberSticker 3 RightViewSticker.cpp";
    if (buttonLinkList.length() != 0)
        buttonLinkList.at(currentLink)->setStyleSheet(editsG);
    qDebug() << "setMemberSticker 4 RightViewSticker.cpp";
    this->resizeEvent(nullptr);
    qDebug() << "End to query View Member " << stickLabelList.size() << QTime::currentTime();
}

//изменения размера компонентов при смене размеров виджета
void ViewStickerR::resizeEvent(QResizeEvent*)
{
    qDebug() << "resizeEvent 1 RightViewSticker.cpp";
    scrollArea->stickerBox->setFixedWidth(scrollArea->width());
    qDebug() << "resizeEvent 2 RightViewSticker.cpp";
    for (int i = 0; i < stickLabelList.length(); i++)
        stickLabelList.at(i)->setFixedHeight(stickLabelList.at(i)->mainLayout->sizeHint().height());
    qDebug() << "resizeEvent 3 RightViewSticker.cpp";
    countLink = scrollArea->height()/downButton->height();
    if (countLink > stickLabelList.length())
        countLink = stickLabelList.length();
    qDebug() << "resizeEvent 4 RightViewSticker.cpp";
    setNewBlock(true);
    qDebug() << "resizeEvent 5 RightViewSticker.cpp";
}

//отлавливаем текущую заметку которая находиться в координатах 0:0
//и отображаеться в левой части текущий
void ViewStickerR::slotTrekEvent()
{
    QTimer::singleShot(400, this, SLOT(slotTrekEvent()));
    if ( ! stickLabelList.isEmpty()){
        int i = 0;
        bool isEnd = false;
        int stickerGroupY = scrollArea->stickerBox->pos().y();
        while ((stickLabelList.length() > i+1) && !isEnd){
            if (stickLabelList.at(i+1)->pos().y() + stickerGroupY < vStickBox->margin())
                i++;
            else
                isEnd = true;
        }
        if ((currentLink != i)&&(i != stickLabelList.length()-1)){
            //buttonLinkList.at(currentLink)->setStyleSheet(fonG);
            //buttonLinkList.at(i)->setStyleSheet(editsG);
            buttonLinkList.at(currentLink)->setChecked(false);
            buttonLinkList.at(i)->setChecked(true);
            currentLink = i;
            if ( ! buttonLinkList.at(i)->isVisible())
                setNewBlock(true);
        }
    }
}

//изменяет текущий блок выбранной заметки если переходим на слудующий блок
//если параметр isDetermineBlock == true, тогда переходим на новый блок
void ViewStickerR::setNewBlock(bool isDetermineBlock)
{
    if (countLink != 0){
        qDebug() << "setNewBlock 1 RightViewSticker.cpp";
        if (isDetermineBlock)
            tekBlock = (int)currentLink/countLink;
        qDebug() << "setNewBlock 2 RightViewSticker.cpp";
        upButton->setVisible(true);
        downButton->setVisible(true);
        fake->setVisible(false);
        qDebug() << "setNewBlock 3 RightViewSticker.cpp";
        for (int i = 0; i < buttonLinkList.length(); i++){
            if ((int)i/countLink == tekBlock)
                buttonLinkList.at(i)->setVisible(true);
            else
                buttonLinkList.at(i)->setVisible(false);
        }
        qDebug() << "setNewBlock 4 RightViewSticker.cpp";
        //если это первый блок, тогда убираем кнопку перейти вверх
        if ((int)currentLink/countLink == 0){
            qDebug() << "setNewBlock 4.1 RightViewSticker.cpp";
            upButton->setVisible(false);
            qDebug() << "setNewBlock 4.2 RightViewSticker.cpp"
                     << buttonLinkList.length() << countLink;
            buttonLinkList.at(countLink-1)->setVisible(true);
            qDebug() << "setNewBlock 4.3 RightViewSticker.cpp";
        }
        qDebug() << "setNewBlock 5 RightViewSticker.cpp";
        //если последний то убираем кнопку перейти вниз
        if ((int)currentLink/countLink == (int)(maxLink-1)/countLink){
            downButton->setVisible(false);
            fake->setVisible(true);
        }
        qDebug() << "setNewBlock 6 RightViewSticker.cpp";
    }
}

//реакция на нажатие кнопок перехода на заметку
void ViewStickerR::slotClickToLinkButton()
{
    QPushButton *select = (QPushButton*)sender();
    //перейти на предыдущий блок
    if (select == upButton){
        if (tekBlock != 0){
            tekBlock--;
            setNewBlock(false);
        }
    }
    //перейти на следующий блок
    else if (select == downButton){
        if (tekBlock != (int)(maxLink-1)/countLink){
            tekBlock++;
            setNewBlock(false);
        }
    }
    //перейти на какой то из кнопок выборки
    else {
        for (int i = 0; i < buttonLinkList.length(); i++)
            if (select == buttonLinkList.at(i)){
                //buttonLinkList.at(currentLink)->setStyleSheet(fonG);
                //buttonLinkList.at(i)->setStyleSheet(editsG);
                buttonLinkList.at(currentLink)->setChecked(false);
                buttonLinkList.at(i)->setChecked(true);
                currentLink = i;
                scrollArea->stickerBox->move(stickLabelList.at(i)->pos().x(),
                                 (-1) * stickLabelList.at(i)->pos().y());
            }
    }
}

//очистка кнопок выбора и всего списка заметок
void ViewStickerR::cleanStickList()
{
    for (int i = 0; i < stickLabelList.length(); i++){
        vStickBox->removeWidget(stickLabelList.at(i));
        delete stickLabelList.at(i);
    }
    stickLabelList.clear();
    for (int i = 0; i < buttonLinkList.length(); i++){
        vLinkBox->removeWidget(buttonLinkList.at(i));
        delete buttonLinkList.at(i);
    }
    buttonLinkList.clear();
}

//вызов изменения заметки, так как была обозначено выполнение заметки
void ViewStickerR::slotCatchCompleteClick(MemberStick *sender)
{
    emit signalGetEditSticker(sender, false);
}

//отправка данной заметки на редактирование
void ViewStickerR::slotSetToEditMod(MemberStick *stick)
{
    emit signalSetToEditSticker(stick);
}

//отправка запроса на отображение заметок
void ViewStickerR::slotGetListNotice()
{
    int choose = -1;
    for (int i = 0; i < typeShowList.length(); i++)
       if (typeShowList.at(i) == (QPushButton*)sender())
           choose = i;
    switch (choose){
    case 0:
        emit signalGetListNotice(0, QStringList{});
        break;
    case 1:
        break;
    case 2:
        break;
    default:;
    }
}
