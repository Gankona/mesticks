#include "editsticker.h"
#include <QDebug>

EditSticker::EditSticker()
{
    //создаем интерфейс списка
    qDebug() << (QChar)9200 << (QChar)9165;
    createListInterface();
    //создаем основной интерфейс
    createOtherInterface();
    qDebug() << size();
}

//устанвливает параметры редактирования для данной заметки
void EditSticker::setStickerToEdit(MemberStick *sendStick)
{
    isNew = false;
    setDefault();
    senderStick = sendStick;
    nameEdit->setText(senderStick->title);
    if (! senderStick->timeEnd.isNull()){
        mainDateTimeEdit->setDateTime(senderStick->timeEnd);
        mainDateTimeCtrlButton->setText("X");
    }

    else {
        mainDateTimeEdit->setDateTime(QDateTime::currentDateTime());
        mainDateTimeCtrlButton->setText("+");
    }
    textEdit->setText(senderStick->text);
    for (int i = 0; i < senderStick->stickList.length(); i++){
        listLineEdit[i]->setText(senderStick->stickList.at(i)->name);
        if (!senderStick->stickList.at(i)->time.isNull()){
            dateTimeEdit[i]->setDateTime(senderStick->stickList.at(i)->time);
            dateTimeCtrlButton[i]->setText("X");
        }
        else {
            dateTimeEdit[i]->setDateTime(QDateTime::currentDateTime());
            dateTimeCtrlButton[i]->setText("+");
        }
        dateTimeCtrlButton[i]->setVisible(true);
        dateTimeEdit[i]->setVisible(true);
        listDeleteButton[i]->setVisible(true);
        listLineEdit[i]->setVisible(true);
    }
    this->resizeEvent(nullptr);
}

//установка параметров что мы создаем новую заметку
void EditSticker::setDefault()
{
    nameEdit->clear();
    textEdit->clear();
    for (int i = 0; i < 9; i++){
        dateTimeCtrlButton[i]->setText("+");
        dateTimeEdit[i]->setDateTime(QDateTime::currentDateTime());
        listLineEdit[i]->clear();
    }
    mainDateTimeCtrlButton->setText("+");
    mainDateTimeEdit->setDateTime(QDateTime::currentDateTime());
}

void EditSticker::createNewStick()
{
    isNew = true;
    setDefault();
    for (int i = 0; i < 9; i++){
        isComplete[i] = false;
        numberList[i] = -1;
    }
    this->resizeEvent(nullptr);
}

//создание списков задач
void EditSticker::createListInterface()
{
    listFrame = new QFrame;
    for (int i = 0; i < 9; i++){
        dateTimeEdit[i] = new QDateTimeEdit;
        dateTimeEdit[i]->setCalendarPopup(true);
        dateTimeEdit[i]->setCalendarWidget(new QCalendarWidget);
        dateTimeEdit[i]->setVisible(false);
        dateTimeEdit[i]->setDisplayFormat("dd/MM/yy hh:mm");
        dateTimeCtrlButton[i] = new QPushButton;
        dateTimeCtrlButton[i]->setVisible(false);
        dateTimeCtrlButton[i]->setFocusPolicy(Qt::NoFocus);
        qDebug() << "EditSticker::createListInterface() 1 " << i;

        listLineEdit[i] = new QLineEdit;
        listLineEdit[i]->setVisible(false);

        listDeleteButton[i] = new QPushButton((QChar)9473);
        listDeleteButton[i]->setVisible(false);
        listDeleteButton[i]->setFocusPolicy(Qt::NoFocus);
        QObject::connect(dateTimeCtrlButton[i], SIGNAL(clicked()), this, SLOT(slotChangeDateTimeStatus()));
        QObject::connect(listDeleteButton[i], SIGNAL(clicked()), this, SLOT(slotDeleteList()));
    }

    listAddButton = new QPushButton("+");
    listAddButton->setFocusPolicy(Qt::NoFocus);
    listAddButton->setFixedSize(30, 30);

    QObject::connect(listAddButton, SIGNAL(clicked()), this, SLOT(slotAddList()));
#ifdef Q_OS_ANDROID
    for (int i = 0; i < 9; i++){
        dateTimeCtrlButton[i]->setFixedHeight(50);
        dateTimeEdit[i]->setFixedHeight(50);
        listLineEdit[i]->setFixedHeight(50);
        listDeleteButton[i]->setFixedHeight(50);
    }
    listAddButton->setFixedHeight(50);

    listFrameLayout = new QGridLayout;
    for (int i = 0; i < 9; i++){
        listFrameLayout->addWidget(dateTimeCtrlButton[i], i, 1);
        listFrameLayout->addWidget(dateTimeEdit[i], i, 0);
        listFrameLayout->addWidget(listLineEdit[i], i, 2);
        listFrameLayout->addWidget(listDeleteButton[i], i, 3);
    }
    listFrameLayout->addWidget(listAddButton, 9, 0);
    listFrame->setLayout(listFrameLayout);
#else
    for (int i = 0; i < 9; i++){
        dateTimeEdit[i]->setParent(listFrame);
        dateTimeCtrlButton[i]->setParent(listFrame);
        listLineEdit[i]->setParent(listFrame);
        listDeleteButton[i]->setParent(listFrame);
    }
    listAddButton->setParent(listFrame);
    listFrame->setParent(this);
#endif
}

void EditSticker::createOtherInterface()
{
    image = nullptr;
    pathToFile = "";
    nameEdit = new QLineEdit;
    nameEdit->setFixedHeight(30);

    textEdit = new QTextEdit;
    textEdit->setMinimumHeight(100);

    senderButton = new QPushButton(tr("сохранить"));
    senderButton->setFocusPolicy(Qt::NoFocus);
    senderButton->setFixedHeight(30);

    imageButton = new QPushButton;
    imageButton->setFocusPolicy(Qt::NoFocus);
    imageButton->setMinimumHeight(150);

    helpLabelNameEdit = new QLabel(tr("Введите название"));
    helpLabelTextEdit = new QLabel(tr("Введите текст заметки"));
    errorMsgLabel = new QLabel;
    //установка седьмой размер шрифта для ошибок
    QFont font = errorMsgLabel->font();
    font.setPointSize(7);
    errorMsgLabel->setFont(font);

    mainDateTimeCtrlButton = new QPushButton("+");
    mainDateTimeCtrlButton->setFocusPolicy(Qt::NoFocus);
    mainDateTimeEdit = new QDateTimeEdit;
    mainDateTimeEdit->setDisplayFormat("dd/MM/yy hh:mm");
    mainDateTimeEdit->setCalendarPopup(true);
    mainDateTimeEdit->setCalendarWidget(new QCalendarWidget);
    mainDateTimeEdit->clear();

    QObject::connect(mainDateTimeCtrlButton, SIGNAL(clicked()), this, SLOT(slotChangeDateTimeStatus()));
    QObject::connect(senderButton, SIGNAL(clicked()), this, SLOT(slotSendToDB()));

#ifdef Q_OS_ANDROID
    vBoxLayout = new QVBoxLayout;
    vBoxLayout->setMargin(5);
    vBoxLayout->setSpacing(5);

    listButton.push_back(new QLabel("Название"));
    listButton.push_back(new QLabel("Время"));
    listButton.push_back(new QLabel("Изображение"));
    listButton.push_back(new QLabel("Список"));
    listButton.push_back(new QLabel("Текст"));

    for (int i = 0; i < listButton.length(); i++){
        exitButton.push_back(new QLabel("X"));
        listButton.last()->setAlignment(Qt::AlignCenter);
        layoutList.push_back(new QHBoxLayout);
        layoutList.last()->addWidget(listButton.at(i));
        layoutList.last()->addWidget(exitButton.last());
    }

    emptyLabel = new QLabel;

    vBoxLayout->addLayout(layoutList.at(0));
    vBoxLayout->addWidget(helpLabelNameEdit);
    vBoxLayout->addWidget(nameEdit);
    vBoxLayout->addLayout(layoutList.at(1));
    vBoxLayout->addWidget(mainDateTimeEdit);
    vBoxLayout->addWidget(mainDateTimeCtrlButton);
    vBoxLayout->addLayout(layoutList.at(2));
    vBoxLayout->addWidget(imageButton);
    vBoxLayout->addLayout(layoutList.at(3));
    vBoxLayout->addWidget(listFrame);
    vBoxLayout->addLayout(layoutList.at(4));
    vBoxLayout->addWidget(helpLabelTextEdit);
    vBoxLayout->addWidget(textEdit);
    vBoxLayout->addWidget(emptyLabel);
    vBoxLayout->addWidget(senderButton);
    vBoxLayout->addWidget(errorMsgLabel);

    allHide();
    foreach (QLabel* b, listButton)
        b->setVisible(true);
    senderButton->setVisible(true);
    errorMsgLabel->setVisible(true);

    errorMsgLabel->setFixedHeight(50);
    senderButton->setFixedHeight(75);
    helpLabelNameEdit->setFixedHeight(50);
    nameEdit->setFixedHeight(50);
    mainDateTimeEdit->setFixedHeight(50);
    mainDateTimeCtrlButton->setFixedHeight(75);
    helpLabelTextEdit->setFixedHeight(50);

    this->setLayout(vBoxLayout);
#else
    qDebug() << "EditSticker::createOtherInterface() 5";
    helpLabelTextEdit->setParent(this);
    helpLabelNameEdit->setParent(this);
    mainDateTimeCtrlButton->setParent(this);
    mainDateTimeEdit->setParent(this);
    qDebug() << "EditSticker::createOtherInterface() 6";
    errorMsgLabel->setParent(this);
    senderButton->setParent(this);
    //imageButton->setParent(this);
    nameEdit->setParent(this);
    textEdit->setParent(this);
#endif
}

//добавляет новую задачу в список
void EditSticker::slotAddList()
{
    for (int i = 0; i < 9; i++)
        if (! dateTimeEdit[i]->isVisible()){
            dateTimeCtrlButton[i]->setVisible(true);
            dateTimeEdit[i]->setVisible(true);
            listDeleteButton[i]->setVisible(true);
            listLineEdit[i]->setVisible(true);
            this->resizeEvent(nullptr);
            i = 10;
        }
}

//удаляет задачу из списка
void EditSticker::slotDeleteList()
{
    QPushButton *select = (QPushButton*)sender();
    for (int i = 0; i < 9; i++)
        if (listDeleteButton[i] == select){
            dateTimeCtrlButton[i]->setVisible(false);
            dateTimeCtrlButton[i]->setText("+");
            dateTimeEdit[i]->setVisible(false);
            dateTimeEdit[i]->clear();
            listDeleteButton[i]->setVisible(false);
            listLineEdit[i]->setVisible(false);
            this->resizeEvent(nullptr);
            isComplete[i] = false;
        }
}

//изменить статус времени и даты
//если + тогда не выбрано иначе выбрано
void EditSticker::slotChangeDateTimeStatus()
{
    if (mainDateTimeCtrlButton == (QPushButton*)sender()){
        if (mainDateTimeCtrlButton->text() == "+")
            mainDateTimeCtrlButton->setText("X");
        else
            mainDateTimeCtrlButton->setText("+");
    }
    else {
        for (int i = 0; i < 9; i++)
            if (dateTimeCtrlButton[i] == (QPushButton*)sender()){
                if (dateTimeCtrlButton[i]->text() == "+")
                    dateTimeCtrlButton[i]->setText("X");
                else
                    dateTimeCtrlButton[i]->setText("+");
            }
    }
}

//отправить данные в базу данных, вызываеться нажатием кнопки отправки
void EditSticker::slotSendToDB()
{
    bool isAllComplete = true;
    if (nameEdit->text().isEmpty()){
        helpLabelNameEdit->setText(tr("Введите название") + (QString)(QChar)9888);
        errorMsgLabel->setText(tr("Поле название должно быть заполнено"));
        isAllComplete = false;
    }
    for (int i = 0; i < 9; i++)
        if (listLineEdit[i]->isVisible())
            if (listLineEdit[i]->text().isEmpty()){
                isAllComplete = false;
                errorMsgLabel->setText(tr("В списке №") + QString::number(i+1)
                                       + " " + tr("отсутствует название."));
            }
    //проверка на заполненность полей
    if (isAllComplete) {
        int type = 0;
        helpLabelNameEdit->setText(tr("Введите название"));
        helpLabelNameEdit->setToolTip(tr("Заглавное название заметки"));
        MemberStick *sender = new MemberStick;
        sender->title = nameEdit->text();
        if (image != nullptr){
            type++;
        }
        if (!textEdit->toPlainText().isEmpty()){
            sender->text = textEdit->toPlainText();
            type += 4;
        }
        sender->timeStart = QDateTime::currentDateTime();
        if (mainDateTimeCtrlButton->text() == "X"){
            sender->timeEnd = mainDateTimeEdit->dateTime();
            type += 8;
        }
        //создания обьекта MemberStick для отправки в БД
        for (int i = 0; i < 9; i++)
            if (listLineEdit[i]->isVisible()){
                sender->stickList.push_back(new StickList(listLineEdit[i]->text(),
                                                          dateTimeEdit[i]->dateTime(), isComplete[i]));
                sender->stickList.last()->id = numberList[i];
            }
        if (!sender->stickList.isEmpty())
            type += 2;
        sender->type = type;
        if (isNew)
            emit signalCreateNewStick(sender);
        else {
            sender->number = senderStick->number;
            qDebug() << "editSticker.cpp id = " << sender->number;
            emit signalEditStick(sender, true);
        }
    }
}

//тут будет установка изображение
void EditSticker::slotSetPicture(){}

//изменения размеров компонентов при смене размеров окна
void EditSticker::resizeEvent(QResizeEvent *e)
{
    QSize newSize;
    if (e == nullptr)
        newSize = this->size();
    else
        newSize = e->size();
#ifdef Q_OS_ANDROID
    foreach (QLabel* b, listButton)
        b->setFixedHeight((newSize.height()-125)/exitButton.length());
    foreach (QLabel* b, exitButton)
        b->setFixedSize((newSize.height()-125)/exitButton.length(),
                        (newSize.height()-125)/exitButton.length());

#else
    //явное изменение размеров компонентов
    int listFrameHeight = newSize.height()- 220;

    setSizeAndMove(mainDateTimeEdit,  12, 5, 145, 25);
    setSizeAndMove(mainDateTimeCtrlButton,  157, 5,  25, 25);
    setSizeAndMove(helpLabelNameEdit, 190, 5, 140, 25);

    setSizeAndMove(nameEdit, 332,  5, newSize.width() - 342, 25);
    setSizeAndMove(listFrame, 10, 32, newSize.width() - 20/* - 225*/, listFrameHeight+10);

    setSizeAndMove(helpLabelTextEdit,  10, newSize.height() - 170, 200,  15);

    setSizeAndMove(textEdit,          10, newSize.height()-150, newSize.width()-20, 125);
    setSizeAndMove(errorMsgLabel,     10, newSize.height()-20, newSize.width()-120, 20);

    setSizeAndMove(senderButton, newSize.width()-100, newSize.height()-20, 85, 20);

    //setSizeAndMove(imageButton,  newSize.width()-210, 27, 200, newSize.height()- 339);

    int countVisibleList = 0;
    for (int i = 0; i < 9; i++){
        setSizeAndMove(dateTimeEdit[i], 2, i*(listFrameHeight/9+2), 145, listFrameHeight/9);
        setSizeAndMove(dateTimeCtrlButton[i], 149, 2+i*(listFrameHeight/9+2),
                       listFrameHeight/9, listFrameHeight/9);
        if (listFrameHeight/9 < 30)
            setSizeAndMove(listLineEdit[i], listFrameHeight/9+151, 2+i*(listFrameHeight/9+2),
                           listFrame->width()-(listFrameHeight/4+127),  listFrameHeight/9);
        else
            setSizeAndMove(listLineEdit[i], listFrameHeight/9+151,
                           2+(listFrameHeight/9-30)/2+i*(listFrameHeight/9+2),
                           listFrame->width()-(listFrameHeight/4+147), 30);
        setSizeAndMove(listDeleteButton[i], listFrame->width()-(2+listFrameHeight/9),
                       2+i*(listFrameHeight/9+2), listFrameHeight/9, listFrameHeight/9);
        if (dateTimeEdit[i]->isVisible())
            countVisibleList++;
    }
    setSizeAndMove(listAddButton, 2, 8*(listFrameHeight/9+2), listFrameHeight/12, listFrameHeight/12);
    if (countVisibleList == 9)
        listAddButton->setVisible(false);
    else
        listAddButton->setVisible(true);
#endif
}

//установка размеров и расположения передаваемого виджета
void EditSticker::setSizeAndMove(QWidget *widget,
                                      int x, int y,
                                      int w, int h)
{
    widget->setFixedSize(w, h);
    widget->move(x, y);
}

#ifdef Q_OS_ANDROID
//спрятать все виджеты
void EditSticker::allHide()
{
    foreach (QLabel* b, listButton)
        b->setVisible(false);
    foreach (QLabel* b, exitButton)
        b->setVisible(false);
    mainDateTimeCtrlButton->setVisible(false);
    helpLabelTextEdit->setVisible(false);
    helpLabelNameEdit->setVisible(false);
    mainDateTimeEdit->setVisible(false);
    errorMsgLabel->setVisible(false);
    senderButton->setVisible(false);
    imageButton->setVisible(false);
    emptyLabel->setVisible(false);
    listFrame->setVisible(false);
    nameEdit->setVisible(false);
    textEdit->setVisible(false);
}

//реакция на нажатие пункта меню
void EditSticker::clickToListButton(int idSelect)
{
    allHide();
    switch (idSelect){
    //название
    case 0:
        nameEdit->setVisible(true);
        helpLabelNameEdit->setVisible(true);
        emptyLabel->setVisible(true);
        break;
    //время
    case 1:
        mainDateTimeCtrlButton->setVisible(true);
        mainDateTimeEdit->setVisible(true);
        emptyLabel->setVisible(true);
        break;
    //изобюражение
    case 2:
        imageButton->setVisible(true);
        break;
    //список
    case 3:
        listFrame->setVisible(true);
        emptyLabel->setVisible(true);
        break;
    //текст
    case 4:
        helpLabelTextEdit->setVisible(true);
        textEdit->setVisible(true);
        break;
    //кнопка свернуть открытую область
    default:
        foreach (QLabel* b, listButton)
            b->setVisible(true);
        errorMsgLabel->setVisible(true);
        senderButton->setVisible(true);
    }

    //отобразить панель открытой вкладки
    if (idSelect != -1){
        listButton.at(idSelect)->setVisible(true);
        exitButton.at(idSelect)->setVisible(true);
    }
}

//отслеживания нажатия по меню, выбор куда нажали и вызов
void EditSticker::mouseReleaseEvent(QMouseEvent *event)
{
    errorMsgLabel->setText(QString::number(event->pos().x())+" "
                          +QString::number(event->pos().y()));
    QPoint pos = event->pos();
    int ret = -1;
    int current = 0;
    int visible = 0;
    foreach (QLabel *w, listButton){
        if (w->isVisible()){
            if ((w->y() < pos.y())
                    &&(w->y()+w->height() > pos.y())
                    &&(w->x()+w->width() > pos.x())
                    &&(w->x() < pos.x()))
                ret = current;
            visible++;
        }
        current++;
    }
    if (visible <= 1)
        clickToListButton(ret);
}
#endif
