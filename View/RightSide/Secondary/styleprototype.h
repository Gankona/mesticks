#ifndef STYLEPROTOTYPE_H
#define STYLEPROTOTYPE_H

#include <QtCore/QDebug>
#include <QtGui/QMouseEvent>

#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

class StylePrototype : public QLabel
{
    Q_OBJECT
public:
    StylePrototype(QMap <QString, QStringList> themeSet);
    QLabel *label;
    QLineEdit *edit;
    QVBoxLayout *vBox;
    QPushButton *button;
    QMap <QString, QStringList> theme;

    void mouseReleaseEvent(QMouseEvent *);

public slots:    
    void slotChooseStyle();

signals:
    void signalSetStyle(QString);
};

#endif // STYLEPROTOTYPE_H
