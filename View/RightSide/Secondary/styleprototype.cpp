#include "styleprototype.h"

StylePrototype::StylePrototype(QMap<QString, QStringList> themeSet)
{
    //создаеться прототип для отображения как будет выглядить виджет
    //с возможным стилем, включает кнопку, поле воода и текст
    theme = themeSet;
    this->setStyleSheet(theme.value(theme.firstKey()).at(0));

    label = new QLabel(theme.lastKey());

    edit = new QLineEdit;

    button = new QPushButton(tr("кнопка"));
    button->setFocusPolicy(Qt::NoFocus);

    //установка компоновщика
    vBox = new QVBoxLayout;
    vBox->setSpacing(5);
    vBox->setMargin(10);
    vBox->addWidget(label);
    vBox->addWidget(edit);
    vBox->addWidget(button);

    this->setLayout(vBox);

    QObject::connect(button, SIGNAL(clicked()),
                     this,   SLOT  (slotChooseStyle()));
}

//отлавливает нажатие на прототип, вызов смены стиля
void StylePrototype::mouseReleaseEvent(QMouseEvent *)
{
    slotChooseStyle();
}

//вызов смены стиля при нажатии на кнопку
void StylePrototype::slotChooseStyle()
{
    emit signalSetStyle(theme.firstKey());
}
