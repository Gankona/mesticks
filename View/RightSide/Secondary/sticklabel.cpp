#include "sticklabel.h"

StickLabel::StickLabel(MemberStick *member)
{
    //создание интерфейса заметки и заполнение ёё данными
    number = member->number;
    thisMember = member;
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    this->resize(10, 10);
    //название заметки
    titleLabel = new QLabel(member->title);
    titleLabel->setFixedHeight(20);
    titleLabel->setAlignment(Qt::AlignCenter);
    titleLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);

    //установка даты
    qDebug() << member->timeStart;
    dataStartLabel = new QLabel(tr("Создано:") + " " + member->timeStart.toString("ddd dd-mm-yy, hh:mm"));
    if (! member->timeEnd.isNull()){
        dataStartLabel->setText(dataStartLabel->text() + '\n' + tr("Окончание: ")
                                +member->timeEnd.toString("ddd dd-mm-yy, hh:mm"));
        dataStartLabel->setFixedHeight(40);
    }
    else
        dataStartLabel->setFixedHeight(20);
    dataStartLabel->setToolTip(member->timeStart.toString());
    dataStartLabel->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

    //установка текста
    textLabel = new QLabel("    " + tr("Текст Заметки") + '\n' + member->text);
    textLabel->setWordWrap(true);
    textLabel->setMargin(5);
    textLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);

    //установка изображения
    imageLabel = new QLabel;
    imageLabel->setMinimumHeight(70);

    mainCheck = new StickCheckBox(tr("выполнить"), member->isCompleteMestick);
    mainCheck->setFixedSize(120, 20);

    //кнопка редактировать
    editButton = new QPushButton((QChar)9998);
    editButton->setFixedSize(20, 20);
    QFont font = this->font();
    font.setPointSize(20);
    editButton->setFont(font);

    QObject::connect(editButton, SIGNAL(clicked()), this, SLOT(slotClickToEdit()));

    //компоновщик
    upBox1 = new QHBoxLayout;
    upBox1->setMargin(0);
    upBox1->setSpacing(0);
    upBox1->addWidget(titleLabel);
    upBox1->addWidget(editButton);

    upBox2 = new QHBoxLayout;
    upBox2->setMargin(0);
    upBox2->setSpacing(0);
    upBox2->addWidget(dataStartLabel);
    upBox2->addWidget(mainCheck);

    upBox = new QVBoxLayout;
    upBox->setMargin(0);
    upBox->setSpacing(2);
    upBox->addLayout(upBox1);
    upBox->addLayout(upBox2);

    mainLayout = new QVBoxLayout;
    mainLayout->setMargin(5);
    mainLayout->setSpacing(4);

    listGrid = new QGridLayout;
    listGrid->setContentsMargins(0, 2, 0, 2);
    listGrid->setSpacing(1);

    //установка списка заметки
    for (int i = 0; i < member->stickList.length(); i++){
        if (member->stickList.at(i)->time.toString() != ""){
            listDataLabel.push_back(new QLabel(member->stickList.at(i)->time.toString("ddd dd-mm-yy, hh:mm")));
            listDataLabel.last()->setFixedSize(140, 30);
            listGrid->addWidget(listDataLabel.last(), i, 0);
        }
        listChekBox.push_back(new StickCheckBox(member->stickList.at(i)->name,
                                                member->stickList.at(i)->isComplete));
        listGrid->addWidget(listChekBox.last(), i, 1);
    }

    listImageBox = new QHBoxLayout;
    listImageBox->setMargin(0);
    listImageBox->setSpacing(3);
    listImageBox->addLayout(listGrid);
    listImageBox->addWidget(imageLabel);

    if (member->image == nullptr){
        //imageLabel->setText("Изображение не найдено");
        //imageLabel->setWordWrap(true);
        //imageLabel->setAlignment(Qt::AlignCenter);
        imageLabel->setVisible(false);
        for (int i = 0; i < listChekBox.length(); i++)
            listChekBox.at(i)->setOrientalLeftToRight();
    }

    listFrame = new QFrame;
    listFrame->setLayout(listImageBox);
    listFrame->setFixedHeight(listImageBox->sizeHint().height());

    //закрепление компоновщика за виджетом
    mainLayout->addLayout(upBox);
    mainLayout->addWidget(listFrame);
    mainLayout->addWidget(textLabel);

    this->setLayout(mainLayout);
}

//отлавливаем нажатие на кнопку выполнить, изменяем параметры заметки
//и отправляем сигнал что бы изменения отправились в БД
void StickLabel::slotClickToCheckBox()
{
    StickCheckBox *select = (StickCheckBox*)sender();
    if (mainCheck == select)
        thisMember->isCompleteMestick = select->isChoose;
    else
        for (int i = 0; i < listChekBox.length(); i++)
            if (listChekBox.at(i) == select)
                thisMember->stickList.at(i)->isComplete = select->isChoose;
    emit signalGetEdit(thisMember);
}

//отлавливаем желание изменить заметку, переходим в режим редактирования
void StickLabel::slotClickToEdit()
{
    emit signalSetToEditMod(thisMember);
}
