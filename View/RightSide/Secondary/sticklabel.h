#ifndef STICKLABEL_H
#define STICKLABEL_H

#include <Model/memberstick.h>
#include <View/StickWidget/stickcheckbox.h>

#include <QtCore/QDebug>
#include <QtWidgets/QLabel>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QPushButton>

class StickLabel : public QFrame
{
    Q_OBJECT
public:
    StickLabel(MemberStick *member);
    MemberStick *thisMember;
    QLabel *textLabel;
    QLabel *titleLabel;
    QLabel *dataStartLabel;
    QPushButton *editButton;
    StickCheckBox *mainCheck;
    QVBoxLayout *upBox;
    QHBoxLayout *upBox1;
    QHBoxLayout *upBox2;
    QVBoxLayout *mainLayout;

    QFrame *listFrame;
    QLabel *imageLabel;
    QGridLayout *listGrid;
    QHBoxLayout *listImageBox;
    QList <QLabel*> listDataLabel;
    QList <QLabel*> listNameLabel;
    QList <StickCheckBox*> listChekBox;

    int number;

public slots:
    void slotClickToEdit();
    void slotClickToCheckBox();

signals:
    void signalGetEdit(MemberStick*);
    void signalSetToEditMod(MemberStick*);
};

#endif // STICKLABEL_H
