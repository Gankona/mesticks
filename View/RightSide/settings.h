#ifndef SETTINGS_H
#define SETTINGS_H

#include <View/MainInterface/styleconfig.h>
#include <View/RightSide/Secondary/styleprototype.h>

#include <QtCore/QDebug>
#include <QtGui/QResizeEvent>

#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QGridLayout>

class Settings : public QTabWidget
{
    Q_OBJECT
public:
    Settings(StyleConfig *wgt);
    StyleConfig *style;
    StyleConfig *widget;
    QList <StylePrototype*> prototype;

    QVBoxLayout *vBox;
    QGridLayout *grid;
    QLabel *nameWindow;
    QLabel *emptyLabel;

    QFrame *styleFrame;
    QFrame *mouseCtrlFrame;
    QFrame *sizeCtrlFrame;

    int column;

    void createStyle();
    void createMouseCtrlFrame();
    void createSizeCtrlFrame();

    void resizeEvent(QResizeEvent *event);

public slots:
    void slotSetNewStyle(QString newStyle);
};

#endif // SETTINGS_H
