#include "about.h"

About::About()
{
    //QPalette pal;
    //pal.setColor(backgroundRole(), Qt::blue);
    //пока тут какое то описание, потом что добавить нужно
    this->setMargin(10);
    this->setText(tr("Memory Sticks\n"
                  "Это программа для ведения учета заметок "
                  "и напоминаний, установка будильников на заданное время"
                  " по заданым событиям.\n \n"
                  "Может потом будет еще что нибуть полезное добавлено"
                  ", но пока мне лень что то придумывать, так что простите.\n \n"
                  "Спасибо что вообще читаете, \n"
                  "разработчик данной программы: Андрей Феленко"));
    this->setAlignment(Qt::AlignTop | Qt::AlignLeft);
    this->setWordWrap(true);
}
