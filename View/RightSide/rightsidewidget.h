#ifndef RIGHTSIDEWIDGET_H
#define RIGHTSIDEWIDGET_H

#include <View/MainInterface/styleconfig.h>
#include <View/RightSide/about.h>
#include <View/RightSide/editsticker.h>
#include <View/RightSide/settings.h>
#include <View/RightSide/viewstickerR.h>

#include <QtCore/QDebug>
#include <QtWidgets/QLabel>
#include <QtWidgets/QStackedWidget>

class RightSideWidget : public QStackedWidget
{
    Q_OBJECT
public:
    RightSideWidget(StyleConfig *style);

    Settings *settingR;
    About *about;

    ViewStickerR *viewSticker;
    EditSticker *editSticker;

public slots:
    void slotAddNewStick();
    void slotSetWidgetFocus(int i);
    void slotSetToEditSticker(MemberStick *stick);
};

#endif // RIGHTSIDEWIDGET_H
