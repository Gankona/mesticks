#ifndef VIEWSTICKER_H
#define VIEWSTICKER_H

#include <Model/memberstick.h>
#include <View/StickWidget/stickscrollarea.h>
#include <View/RightSide/Secondary/sticklabel.h>

#include <QtCore/QTimer>
#include <QtGui/QResizeEvent>

#include <QtWidgets/QCheckBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QGridLayout>

class ViewStickerR : public QLabel
{
    Q_OBJECT
public:
    ViewStickerR();
    QLabel *upLabel;
    QLabel *fake;
    QLabel *fakeList;
    QList <StickLabel*> stickLabelList;
    QList <QPushButton*> buttonLinkList;
    QList <QPushButton*> typeShowList;
    QPushButton *upButton;
    QPushButton *downButton;
    StickScrollArea *scrollArea;
    QGridLayout *vLinkBox;
    QVBoxLayout *vStickBox;
    QGridLayout *typeShowLayout;
    QGridLayout *mainGrid;
    QList <MemberStick*> listMember;

    QString fonG;
    QString buttonG;
    QString editsG;

    int countLink;
    int currentLink;
    int maxLink;
    int tekBlock;
    int currentStyleStick;

    void resizeEvent (QResizeEvent *event);
    void setMemberStick (QList <MemberStick*> member);
    void setNewBlock (bool isDetermineBlock);
    void cleanStickList();

public slots:
    void slotTrekEvent();
    void slotGetListNotice();
    void slotClickToLinkButton();
    void slotCatchCompleteClick (MemberStick *sender);
    void slotSetToEditMod (MemberStick *stick);

signals:
    void signalGetEditSticker   (MemberStick*, bool);
    void signalSetToEditSticker (MemberStick*);
    void signalGetListNotice    (int, QStringList);
};

#endif // VIEWSTICKER_H
