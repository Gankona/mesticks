#include "settings.h"

Settings::Settings(StyleConfig *wgt)
{
    //создание интерфейса настроек
    prototype.clear();
    widget = wgt;

    createStyle();
    createMouseCtrlFrame();
    createSizeCtrlFrame();

    this->addTab(styleFrame, QIcon(":/images/Files/Images/style.jpg"), "style");
    this->addTab(mouseCtrlFrame, QIcon(":/images/Files/Images/mouse.gif"), "mouse");
    this->addTab(sizeCtrlFrame, QIcon(":/images/Files/Images/size.jpg"), "size");

    this->setFocusPolicy(Qt::NoFocus);
    //this->setTabPosition(QTabWidget::West);
}

void Settings::createMouseCtrlFrame()
{
    mouseCtrlFrame = new QFrame;
}

void Settings::createSizeCtrlFrame()
{
    sizeCtrlFrame = new QFrame;
}

void Settings::createStyle()
{
    styleFrame = new QFrame;
    style = new StyleConfig;

    foreach (QString theme, style->mapTheme.keys())
       prototype.push_back(new StylePrototype(style->setColorTheme(theme, false)));

    emptyLabel = new QLabel;

    vBox = new QVBoxLayout;
    vBox->setSpacing(5);
    vBox->setMargin(5);

    nameWindow = new QLabel(tr("Выбор стиля приложения"));
    nameWindow->setFixedHeight(25);
    nameWindow->setAlignment(Qt::AlignCenter);
    grid = new QGridLayout;
    grid->setMargin(10);
    grid->setSpacing(20);

    //отображение прототипов стилей
    for (int i = 0; i < prototype.length(); i++){
        QObject::connect(prototype.at(i), SIGNAL(signalSetStyle(QString)),
                         this, SLOT(slotSetNewStyle(QString)));
        prototype.at(i)->setMinimumHeight(120);
        prototype.at(i)->setMaximumHeight(200);
        grid->addWidget(prototype.at(i), i/3, i%3);
    }

    //установка компоновщика на виджет
    vBox->addWidget(nameWindow);
    vBox->addLayout(grid);
    vBox->addWidget(emptyLabel);

    column = 3;
    styleFrame->setLayout(vBox);
}

//установка нового стиля данному окну
void Settings::slotSetNewStyle(QString newStyle)
{
    widget->setColorTheme(newStyle, true);
}

//изменения размера компонента при смене размера виджета
void Settings::resizeEvent(QResizeEvent *event)
{
    int columnEvent = event->size().width()/120;
    if (columnEvent != column){
        column = columnEvent;
        for (int i = 0; i < prototype.length(); i++)
            grid->removeWidget(prototype.at(i));
        for (int i = 0; i < prototype.length(); i++)
            grid->addWidget(prototype.at(i), i/column + 1, i%column + 1);
    }
}
