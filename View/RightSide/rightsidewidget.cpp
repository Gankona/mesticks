#include "rightsidewidget.h"

RightSideWidget::RightSideWidget(StyleConfig *style)
{
    //установка минимальных размеров правой части окна
    qDebug() << "1 RightSideWidget";
    this->setMinimumHeight(500);
    this->setMinimumWidth(600);

    //создания окон просмотра, редактирования настроек и инфо
    //и запихивание их в стек
    settingR = new Settings(style);
    about = new About;
    viewSticker = new ViewStickerR;
    editSticker = new EditSticker;
    this->addWidget(about);
    this->addWidget(editSticker);
    this->addWidget(viewSticker);
    this->addWidget(settingR);

    this->setCurrentIndex(0);

    QObject::connect(viewSticker, SIGNAL(signalSetToEditSticker(MemberStick*)),
                     this, SLOT(slotSetToEditSticker(MemberStick*)));
}

//установка нового виджета в фокус
void RightSideWidget::slotSetWidgetFocus(int i)
{
#ifdef Q_OS_ANDROID
    this->setVisible(true);
#endif
    if (i != 2)
        viewSticker->cleanStickList();
    if (this->currentWidget() == editSticker)
        editSticker->setDefault();
    if (this->currentIndex() == i)
        this->setCurrentIndex(0);
    else
        this->setCurrentIndex(i);
}

//добавление новой заметки, при этом открываеться окно редактирования заметок
void RightSideWidget::slotAddNewStick()
{
    qDebug() << "RightSideWidget::slotAddNewStick() begin";
    editSticker->createNewStick();
    this->setCurrentWidget(editSticker);
    this->setVisible(true);
    qDebug() << "RightSideWidget::slotAddNewStick() end";
}

//редактирование заметок, окно редактирования переходит в фокус
void RightSideWidget::slotSetToEditSticker(MemberStick* stick)
{
    editSticker->setStickerToEdit(stick);
    viewSticker->cleanStickList();
    this->setCurrentWidget(editSticker);
}
