#ifndef EDITSTICKER_H
#define EDITSTICKER_H

#include <Model/memberstick.h>
#include <View/RightSide/Secondary/sticklabel.h>

#include <QtCore/QTime>
#include <QtCore/QDebug>
#include <QtGui/QImage>
#include <QtGui/QMouseEvent>
#include <QtGui/QResizeEvent>

#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>

class EditSticker : public QFrame
{
    Q_OBJECT
public:
    EditSticker();
    MemberStick *senderStick;
    QStringList newStyle;

    QDateTimeEdit *dateTimeEdit[9];
    QLineEdit *listLineEdit[9];
    QPushButton *dateTimeCtrlButton[9];
    QPushButton *listDeleteButton[9];
    QPushButton *listAddButton;
    QFrame *listFrame;

    QTextEdit *textEdit;
    QLineEdit *nameEdit;
    QLabel *helpLabelNameEdit;
    QLabel *helpLabelTextEdit;
    QPushButton *senderButton;
    QPushButton *imageButton;
    QLabel *errorMsgLabel;

    QDateTimeEdit *mainDateTimeEdit;
    QPushButton *mainDateTimeCtrlButton;

    bool isNew;
    bool isComplete[];
    int editNumberOfThisStick;
    int numberList[9];
    QString pathToFile;
    QImage *image;

    void createListInterface();
    void createOtherInterface();
    void resizeEvent(QResizeEvent *e);
    void setSizeAndMove(QWidget *widget,
                             int x, int y,
                             int w, int h);
    void setDefault();
    void setStickerToEdit(MemberStick *sendStick);
    void createNewStick();
#ifdef Q_OS_ANDROID
    QVBoxLayout *vBoxLayout;
    QLabel *emptyLabel;
    QGridLayout *listFrameLayout;
    QList <QLabel*> listButton;
    QList <QLabel*> exitButton;
    QList <QHBoxLayout*> layoutList;
    void allHide();
    void clickToListButton(int idSelect);
    void mouseReleaseEvent(QMouseEvent *event);
#endif

public slots:
    void slotSendToDB();
    void slotAddList();
    void slotDeleteList();
    void slotSetPicture();
    void slotChangeDateTimeStatus();

signals:
    void signalCreateNewStick(MemberStick*);
    void signalEditStick(MemberStick*, bool);
};

#endif // EDITSTICKER_H
