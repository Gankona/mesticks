#ifndef ABOUT_H
#define ABOUT_H

#include <QtCore/QDebug>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QLabel>

class About : public QLabel
{
    Q_OBJECT
public:
    About();
};

#endif // ABOUT_H
